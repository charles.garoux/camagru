<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');

function simple_sanitize_str($value)
{
	return (trim(htmlspecialchars($value)));
}

/*
** multi_sanitize_check_value()
** Prend toutes les entrées dans un array formaté (voir ci dessous). Nettoie les entrées et les verifie.
** Attention : L'utilisation de $_GET ou $_POST avec des champs inexistant dans la requet peu generer un crash.
**
** Format de l'array :
	[
	'inputName' => ['value' => $_POST['inputName'], 'type' => 'str'],
	'inputName2' => ['value' => $_GET['inputName2'], 'type' => 'email']
	]
** Exemple :
$inputsList = [
	'login' => ['value' => ($_POST['login'] ?? NULL), 'type' => 'login'],
	'password' => ['value' => ($_POST['password'] ?? NULL), 'type' => 'str'],
	'email' => ['value' => ($_POST['email'] ?? NULL), 'type' => 'email'],
	'receiveEmail' => ['value' => ($_POST['receiveEmail'] ?? NULL), 'type' => 'bool']
];
*/

function multi_sanitize_check_value(&$inputsList)
{
	foreach ($inputsList as $key => $check) {
		$check['value'] = simple_sanitize_str($check['value']);
		$type = $check['type'];
		try {
			$inputsList[$key]['value'] = Input::$type($check['value']);
		} catch (Exception $exception) {
			print_debug_user_input(__FILE__, __LINE__, __FUNCTION__ .'() : clé "'. $key .'" valeur "'. $check['value'] .'", '. $exception->getMessage());
			return (false);
		}
	}
	return (true);
}

function check_input(&$inputsList, $requestType)
{
	try {
		Input::check($inputsList, $requestType);
	} catch (Exception $exception) {
		if ($exception->getCode() === 900) {
			print_to_user('Warning', 'The request is incomplete');
		}
		print_debug_user_input(__FILE__, (__LINE__ - 4), $exception->getMessage());
		return (false);
	}

	if (multi_sanitize_check_value($inputsList) === false) {
		print_debug_user_input(__FILE__, (__LINE__), 'multi_sanitize_check_value() === false');
		return (false);
	}
	return (true);
}

?>
