<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/key_generator.php');

/*
** Verification du fichier de journal en cas d'utilisatation.
** Le chemin du fichier est indique par variable d'environnment "LOG_FILE_PATH"
** Ce fichier est utilise quand la variable d'environnment "DEBUG" est egal a "LOG" et "SENDING_MAIL" est egal a "DEBUG"
**
** Un identifiant est cree pour identifier la requete concernée
*/

if (env('DEBUG') == 'LOG' || env('SENDING_MAIL') == 'DEBUG') {
	$REQUEST_ID = key_generator(env('REQUEST_ID_LENGTH')) .' '. date("d-m-Y_H:i:s") .' '. $_SERVER['REQUEST_URI'];
	$LOG_FILE = env('LOG_FILE_PATH');

	if (file_exists($LOG_FILE) === False) {
		echo "[ERREUR] ".__FILE__." :Le fichier de journal \"$LOG_FILE\" n'existe pas. Il doit etre creer pour faire fonctionner le mode \"SENDING_MAIL\" en \"DEBUG\" ou le mode \"DEBUG\" en \"LOG\" " . PHP_EOL;
		exit();
	}
	if (is_writable($LOG_FILE) === false) {
		echo "[ERREUR] ".__FILE__." :Le fichier de journal \"$LOG_FILE\" ne peu pas etre ecrit" . PHP_EOL;
		exit();
	}

	if(!function_exists('get_number_pages')) {
		function write_in_log($message_log)
		{
			global $REQUEST_ID;
			global $LOG_FILE;

			if (file_put_contents($LOG_FILE, '['. $REQUEST_ID .'] '. $message_log, FILE_APPEND) === false) {
				echo '[ERREUR] '.__FILE__.' print_debug()'. (__LINE__ - 1) .': Impossible d\'ecrire dans le ficher de journal '. $LOG_FILE . PHP_EOL;
				exit();
			}
		}
	}
}

?>
