<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');

if(!function_exists('app_redirection')) {

  function app_redirection($in_app_target)
  {
		header('Location: '. env('APP_URL') . $in_app_target);
  }
}
?>
