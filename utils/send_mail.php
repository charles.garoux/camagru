<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/key_generator.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/write_log.php');


if (env('SENDING_MAIL') == 'DEBUG') {
	$REQUEST_ID = key_generator(env('REQUEST_ID_LENGTH')) .' '. date("d-m-Y_H:i:s") .' '. $_SERVER['REQUEST_URI'];
	$LOG_FILE = env('LOG_FILE_PATH');

	if (file_exists($LOG_FILE) === False) {
		echo "[ERREUR] ".__FILE__." :Le fichier de journal \"$LOG_FILE\" n'existe pas. Il doit etre creer pour faire fonctionner le mode \"SENDING_MAIL\" en \"DEBUG\" " . PHP_EOL;
		exit();
	}
	if (is_writable($LOG_FILE) === false) {
		echo "[ERREUR] ".__FILE__." :Le fichier de journal \"$LOG_FILE\" ne peu pas etre ecrit" . PHP_EOL;
		exit();
	}
}

function send_mail($to, $subject, $message)
{
	$header = "From: Camabot <camagru.mail.test@gmail.com>";

	if (env('SENDING_MAIL') == 'TRUE') {
		if (($res = mail($to, $subject, $message, $header)) === false) {
			$error = error_get_last();
			print_debug_mail(__FILE__, (__LINE__ - 1), __FUNCTION__, 'send_mail() => "'. $error['message'] .'"');
			return (false);
		}
	}
	else if (env('SENDING_MAIL') == 'DEBUG') {
		global $REQUEST_ID;
		global $LOG_FILE;

		$message_log = "================". PHP_EOL;
		$message_log = $message_log ."to : $to". PHP_EOL;
		$message_log = $message_log ."subject : $subject". PHP_EOL;
		$message_log = $message_log ."header : $header". PHP_EOL;
		$message_log = $message_log ."subject : $subject". PHP_EOL;
		$message_log = $message_log ."message : $message". PHP_EOL;
		$message_log = $message_log ."==========================================================================". PHP_EOL;

		if (write_in_log($message_log) === false) {
			echo '[ERREUR] '.__FILE__.' write_in_log()'. (__LINE__ - 1) . PHP_EOL;
			exit();
		}
	}

	return (true);
}

?>
