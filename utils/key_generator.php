<?php

function key_generator($len = 40)
{
    $charList = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charListLen = strlen($charList);
    $key = '';
    for ($i = 0; $i < $len; $i++) {
        $key .= $charList[rand(0, $charListLen - 1)];
    }
    return $key;
}

?>
