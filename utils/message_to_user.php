<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/flash_page.php');

/*
** Les types :
**	ATTENTION = avertisement pour l'utilisateur
**
*/

if(!function_exists('print_to_user')) {

  function print_to_user($type, $message, $redirection = null)
  {
		if (env('DEBUG') == 'TRUE') {
			print("[$type] $message". PHP_EOL);
		}
		else {
			print_flash_page($type, $message, $redirection);
		}
  }
}


?>
