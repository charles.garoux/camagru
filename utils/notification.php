<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/send_mail.php');

function notify($email_target, $message)
{
	$subject = 'Notification from Camagru';

	if (send_mail($email_target, $subject, $message) === false) {
		print_debug_mail(__FILE__, __LINE__, __FUNCTION__, 'send_mail()');
		return (false);
	}

	return (true);
}


?>
