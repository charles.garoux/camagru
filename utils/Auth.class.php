<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_connection.php');

class Auth
{

	static public function is_authenticate()
	{
		$is_auth = isset($_SESSION['user']);

		/*
		** Check if the user exist
		*/
		if ($is_auth && db_find_user($_SESSION['user']['login']) === ReturnCase::Empty) {
			unset($_SESSION['user']);
			return (false);
		}

		return ($is_auth);
	}

	static public function sign_in($login, $password)
	{
		if (($user = db_find_user($login)) === ReturnCase::Empty) {
			print_to_user('Warning', 'Unknown login');
			return (false);
		}

		if (db_find_registerTicket_by_idUser($user['id']) !== ReturnCase::Empty) {
			print_to_user('Warning', 'The account has not been validated, the link has been sent to the registration email');
			return (false);
		}

		$passwordHashed = hash(env('HASH_ALGO'), $password);

		if ($passwordHashed != $user['password']) {
			print_to_user('Warning', 'The password is incorrect');
			return (false);
		}

		$_SESSION['user'] = array('id' => $user['id'], 'login' => $user['login']);

		return (true);
	}

	static public function sign_out()
	{
		if (isset($_SESSION['user'])) {
			unset($_SESSION['user']);
		}
	}
}

?>
