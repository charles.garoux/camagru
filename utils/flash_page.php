<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');

if(!function_exists('print_flash_page')) {

  function print_flash_page($type, $message, $redirection = null, $waiting_ms = null)
  {
		if ($waiting_ms === null) {
			$waiting_ms = env('FLASH_WAIT');
		}
		if ($redirection == null) {
			$redirection = "history.go(-1);";
		}
		else {
			$redirection = 'window.location.href="'. $redirection .'";';
		}

		$content = <<< CONTENT
<p>You will be redirected.</p>
CONTENT;

		$script = <<< SCRIPT
		function wait(ms){
			 var start = new Date().getTime();
			 var end = start;
			 while(end < start + ms) {
				 end = new Date().getTime();
			}
		}

		window.onload = function(){
			wait($waiting_ms);
			$redirection
			};
SCRIPT;

		print_template($type, false, $content, $message, $script, false);
  }
}
?>
