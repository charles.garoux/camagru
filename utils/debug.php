<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/key_generator.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/flash_page.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/write_log.php');

if(!function_exists('print_debug')) {

	function print_debug($error_type, $message)
	{
		static $already_display = false;
		$debug_log = "[$error_type] $message". PHP_EOL;

		if (env('DEBUG') == 'TRUE') {
			print ($debug_log);
		}
		else if (env('DEBUG') == 'LOG') {
			global $REQUEST_ID;
			global $LOG_FILE;

			if ($already_display === false) {
				print_flash_page('Error', 'A problem has occurred, its information has been recorded');
				$already_display = true;
			}

			if (write_in_log($debug_log) === false) {
				echo '[ERREUR] '.__FILE__.' write_in_log()'. (__LINE__ - 1) . PHP_EOL;
				exit();
			}
		}
		else {
			if ($already_display === false) {
				print_flash_page('Error', 'A problem has occurred');
				$already_display = true;
			}
		}
  }
}

if(!function_exists('print_debug_db')) {

  function print_debug_db($file, $line, $function, $origin)
  {
	  $functionName = $function ? "$function() " : '';
		print_debug('ERREUR', '[BDD] '. $file .':'. ($line - 1) .' '. $functionName .': '. $origin);
  }
}

if(!function_exists('print_debug_user_input')) {

  function print_debug_user_input($file, $line, $origin)
  {
		print_debug('ERREUR', '[ENTRÉE UTILISATEUR] '. $file .':'. ($line - 1) .' : '. $origin);
  }
}

if(!function_exists('print_debug_mail')) {

  function print_debug_mail($file, $line, $function, $origin)
  {
		print_debug('ERREUR', '[MAIL] '. $file .':'. ($line - 1) .' '. $function .': '. $origin);
  }
}

if(!function_exists('print_debug_routing')) {

  function print_debug_routing($file, $line, $function, $origin)
  {
		print_debug('ERREUR', '[ROUTING] '. $file .':'. ($line - 1) .' '. $function .': '. $origin);
  }
}

if(!function_exists('print_debug_view')) {

  function print_debug_view($file, $line, $function, $origin)
  {
	  $functionName = $function ? "$function() " : '';
		print_debug('ERREUR', '[BDD] '. $file .':'. ($line - 1) .' '. $functionName .': '. $origin);
  }
}

?>
