<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');

if(!function_exists('get_number_pages')) {

	function get_number_pages($nbrElements)
	{
		if (($limit = intval(env('ELEMENTS_LIMIT'))) < 5)
			$limit = 5;
		return (intval(ceil($nbrElements / $limit)));
	}
}
?>
