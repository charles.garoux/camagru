<?php

class Input {
	static $errors = true;

	static function check($arr, $on = false) {
		if ($on === 'inputsList') {
			foreach ($arr as $key => $value) {
				if (empty($arr[$key]['value'])) {
					self::throwError('Information ('. $key .') manquante', 900);
				}
			}
		}
		else {
			if ($on === false) {
				$on = $_REQUEST;
			}
			foreach ($arr as $key => $value) {
				if (empty($on[$key])) {
					self::throwError('Information ('. $key .') manquante', 900);
				}
			}
		}
	}

	static function int($val) {
		$val = filter_var($val, FILTER_VALIDATE_INT);
		if ($val === false) {
			self::throwError('Entier invalide', 901);
		}
		return $val;
	}

	static function str($val) {
		if (!is_string($val)) {
			self::throwError('Chaine invalide', 902);
		}
		$val = trim(htmlspecialchars($val));
		return $val;
	}

	static function bool($val) {
		$val = filter_var($val, FILTER_VALIDATE_BOOLEAN);
		return $val;
	}

	static function email($val) {
		$val = filter_var($val, FILTER_VALIDATE_EMAIL);
		if ($val === false) {
			self::throwError('Email invalide', 903);
		}
		return $val;
	}

	static function url($val) {
		$val = filter_var($val, FILTER_VALIDATE_URL);
		if ($val === false) {
			self::throwError('URL invalide', 904);
		}
		return $val;
	}

	static function base64($val) {
		$check = base64_encode(base64_decode($val));
		if ($val != $check || $check === false) {
			self::throwError('base64 invalide', 905);
		}
		return $check;
	}

	static function login($val) {
		if (strlen($val) > 32) {
			print_to_user('Warning', 'Login too long, 32 characters maximum');
			exit();
		}
		return (self::str($val));
	}

	static function comment($val) {

		if (strlen($val) > 142) {
			print_to_user('Warning', 'Comment too long, 140 characters maximum');
			exit();
		}
		return (self::str($val));
	}

	/*
	** La valeur 1309796 a ete trouve empiriquement
	** suite a de nombreux essaie
	*/
	static function imageBase64($val) {
		if (strlen($val) >= 1309796) {
			print_to_user('Warning', 'Image too heavy');
			exit();
		}
		return (self::base64($val));
	}

	static function throwError($error = 'Error In Processing', $errorCode = 0) {
		if (self::$errors === true) {
			throw new Exception($error, $errorCode);
		}
	}
}

?>
