<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');

if(!function_exists('get_nav')) {
  function get_nav()
  {
		global $ROUTES_LIST;

		$app_name = env('APP_NAME');
		$nav_list = null;
		$button_list = null;

		foreach ($ROUTES_LIST as $route_name => $data) {
			if ($data['nav_bar'] === true && $data['nav_bar'] !== 'button' && ($data['need_auth'] === false || $data['need_auth'] === Auth::is_authenticate())) {
				$link = $data['link'];
				$nav_element = <<< NAV_LIST
				<li class="nav-item">
					<a class="nav-link" href="$link">$route_name</a>
				</li>
NAV_LIST;
				$nav_list = $nav_list . $nav_element;
			}
		}

		foreach ($ROUTES_LIST as $route_name => $data) {
			if ($data['nav_bar'] === 'button' && $data['need_auth'] === Auth::is_authenticate()) {
				$link = $data['link'];
				$button_element = <<< BUTTON_LIST
				<a class="btn btn-primary" href="$link">$route_name</a>
BUTTON_LIST;
				$button_list = $button_list . $button_element;
			}
		}

		if (isset($_SESSION['user']))
			$loginElement = '<span class="btn alert-info mr-1">'. $_SESSION['user']['login'] .'</span>';
		else
			$loginElement = '';

$script = <<< SCRIPT
function toogleNavbar() {
	document.getElementById("navbarSupportedContent").classList.toggle('collapse');
}

document.getElementById("toggle-navigation").addEventListener("click", toogleNavbar);
SCRIPT;

		return <<< NAV_BAR
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		    <h1 class="navbar-brand">$app_name</h1>
		    <button id="toggle-navigation" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		      <span class="navbar-toggler-icon"></span>
		    </button>

		    <div class="collapse navbar-collapse" id="navbarSupportedContent">
		      <ul class="navbar-nav mr-auto">
						$nav_list
		      </ul>
					<div class="my-2 my-lg-0">
						$loginElement
						$button_list
					</div>
		    </div>
		  </nav>
			<script>
				$script
			</script>
NAV_BAR;
  }
}
?>
