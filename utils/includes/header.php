<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');

if(!function_exists('get_header')) {
  function get_header($header_title = null, $type = null)
  {
		if ($header_title == null) {
			return (null);
		}

		$colorHeaderClass = "";
		if ($type != null) {
			if ($type == "Success") {
				$colorHeaderClass = "alert alert-success";
			}
			else if ($type == "Warning") {
				$colorHeaderClass = "alert alert-warning";
			}
			else if ($type == "Error") {
				$colorHeaderClass = "alert alert-danger";
			}
		}

		return <<< HEADER
		<div class="col-12">
			<div class="card bg-white px-3 my-3 $colorHeaderClass">
	      <div class="card-body">
					<h1>$header_title</h1>
	      </div>
	    </div>
		</div>
HEADER;
  }
}
?>
