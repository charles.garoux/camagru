<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/nav.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/header.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/footer.php');

if(!function_exists('print_template')) {

  function print_template($window_title, $with_nav_bar, $content, $header_title = null, $script = null, $with_footer = true)
  {
	  	$faviconPath = env('APP_URL') .'/favicon.ico';

		if ($header_title != null)
			$header = get_header($header_title, $window_title);
		else
			$header = null;

		if ($with_nav_bar)
			$nav_bar = get_nav();
		else
			$nav_bar = null;

		if ($with_footer)
			$footer = get_footer();
		else
			$footer = null;

		print <<< HTML_HEADER
		<head>
    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" href="$faviconPath">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>$window_title</title>
  </head>
	<body>
			$nav_bar
			<div class="container-fluid">
				$header
				<main>
					$content
				</main>
				$footer
			</div>
	</body>
	<script>
		$script
	</script>
	<html>
HTML_HEADER;
  }
}
?>
