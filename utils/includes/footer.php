<?php

if(!function_exists('get_footer')) {
  function get_footer()
  {
		return <<< FOOTER
<footer class="text-muted mt-5">
	<hr>
  <div class="container">
    <p class="float-right">
      <a href="#">Back to top</a>
    </p>
    <p>&copy; chgaroux</p>
  </div>
</footer>
FOOTER;
  }
}
?>
