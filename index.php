<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/redirection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');

app_redirection(get_route('Home')['link']);
?>
