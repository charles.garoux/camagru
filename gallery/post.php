<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_posts.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_comments.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_likes.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');

/*
** Recois une requet HTTP avec en GET :
**	idPost (identifiant du post)
**
** Lecture en BDD :
**	Le post correspondant a l'idPost
**	Tous les commentaire du post
**
** Si le post n'existe pas :
**	Afficher un avertissement
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

$inputsList = [
	'idPost' => ['value' => ($_GET['idPost'] ?? NULL), 'type' => 'int']
];

if (check_input($inputsList, $_GET) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$idPost = $inputsList['idPost']['value'];

if (($post = db_find_post($idPost))=== false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_get_all_posts_by_date()');
	exit();
}
else if ($post === ReturnCase::Empty) {
	print_to_user('Warning', 'This post does not exist');
	exit();
}

if (($comments = db_find_all_comments_by_idPost($idPost)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_all_comments_by_idPost()');
	exit();
}
else if ($comments == ReturnCase::Empty) {
	$comments = NULL;
}

if (($nbrLikes = db_count_likes($idPost)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_count_likes()');
	exit();
}

if (Auth::is_authenticate() === true) {
	if (($like = db_find_like($_SESSION['user']['id'], $idPost)) === false) {
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_like()');
		exit();
	}
	else if ($like !== ReturnCase::Empty)
		$isLikeByUser = true;
	else
	$isLikeByUser = false;
}
else
	$isLikeByUser = false;

$DATA_FOR_VIEW = array('post' => $post, 'comments' => $comments, 'nbrLikes' => $nbrLikes, 'isLikeByUser' => $isLikeByUser);

require_once($_SERVER['DOCUMENT_ROOT'] . '/gallery/post.view.php');
?>
