<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');

/*
** Vue d'un post
**
** Les informations du post sont contenue dans l'array $DATA_FOR_VIEW
** (voir /gallery/post.php pour connaitre sont contenue)
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (isset($DATA_FOR_VIEW) === false) {
	print_debug_view(__FILE__, __LINE__, __FUNCTION__, 'manque de donnees pour le fonctionnement de la vue');
	exit();
}

/* ===================== TRAITEMENT DE LA VUE ==================== */

$imageSource = 'data:image/png;base64,'. $DATA_FOR_VIEW['post']['image'];

$likePostLink = get_route_link('Like Post') .'?idPost='. $DATA_FOR_VIEW['post']['id'];
$deletePostLink = get_route_link('Delete Post').'?idPost='. $DATA_FOR_VIEW['post']['id'];

$deleteCommentRoute = get_route_link('Delete Comment');
$userGaleryRoute = get_route_link('User Gallery');

$actionScript = get_route_link('Add Comment');

$post = $DATA_FOR_VIEW['post'];
$nbrLikes = $DATA_FOR_VIEW['nbrLikes'];

if (isset($_SESSION['user']))
	$userId = $_SESSION['user']['id'];
else
	$userId = 0;

/*========== PREPARATION POST ==========*/

if ($post['idUser'] === $userId) {
	$postButton = '<a href="'. $deletePostLink .'" class="btn btn-outline-danger btn-sm">Delete my post</a>';
	$ownerGallery = '';
}
else {
	$ownerGallery = '<a href="'. $userGaleryRoute .'?idUser='. $post['idUser'] .'" class="btn btn-outline-info">Owner\'s gallery</a>';
	if ($DATA_FOR_VIEW['isLikeByUser'])
		$postButton = '<a href="'. $likePostLink .'" class="btn btn-success">I like it</a>';
	else
		$postButton = '<a href="'. $likePostLink .'" class="btn btn-outline-success">Like</a>';
}

if ($nbrLikes > 1)
	$likeInfo = '<span class="btn btn-info">'. $nbrLikes .' likes</span>';
else if ($nbrLikes == 1)
	$likeInfo = '<span class="btn btn-info">'. $nbrLikes .' like</span>';
else
	$likeInfo = '';

$postView = <<< POST
<div class="row">
	<div class="card col-md-6 col-12 offset-md-3 mt-3 px-0">
	  <img src="$imageSource" class="card-img-top">
	  <div class="card-body">
	    <h5 class="card-title">$ownerGallery $likeInfo</h5>
	    $postButton
	  </div>
	</div>
</div>
POST;

/*========== PREPARATION DES COMMENTAIRES ==========*/

$commentsList = '';
if ($DATA_FOR_VIEW['comments']) {
	foreach ($DATA_FOR_VIEW['comments'] as $comment) {
		if ($comment['idUser'] === $userId) {
			$deleteCommentLink = $deleteCommentRoute .'?idComment='. $comment['id'];
			$commentButton = '<a href="'. $deleteCommentLink .'" class="btn btn-outline-danger btn-sm">Delete my comment</a>';
		}
		else {
			$userGaleryLink = $userGaleryRoute .'?idUser='. $comment['idUser'];
			$commentButton = '<a href="'. $userGaleryLink .'" class="btn btn-outline-info btn-sm">See User Gallery</a>';
		}

		$commentsList = $commentsList . <<< COMMENT
		<div class="row">
			<div class="card col-md-6 col-12 offset-md-3 mt-3">
				<div class="card-body">
				<p class="card-text">$comment[text]</p>
				$commentButton
				</div>
			</div>
		</div>
COMMENT;
	}
}

$content = <<< CONTENT
$postView
$commentsList
<form class="row" action="$actionScript" method="post">
	<input type="hidden" name="idPost" value="$post[id]">
	<div class="card col-md-6 col-12 offset-md-3 mt-3">
		<div class="card-body">
			<div class="form-group">
		    <textarea class="form-control" id="text" name="text" placeholder="Your comment ..." rows="2" maxlength="140"></textarea>
		  </div>
			<button type="submit" class="btn btn-primary btn-lg btn-block">Push comment</button>
		</div>
	</div>
</form>
CONTENT;

print_template('Post', true, $content);
?>
