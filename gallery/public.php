<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_pagination.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/pagination.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');

/*
** Recois une requet HTTP avec en GET
**	page	(numero de la page de la galerie)
**
** Lecture en BDD :
**	Tous les posts dans du plus recent a plus ancien
**	(Les 10 utilisateur avec le plus de likes, optionnel)
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

$inputsList = [
	'page' => ['value' => ($_GET['page'] ?? 1), 'type' => 'int']
];

if (check_input($inputsList, 'inputsList') === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$numPage = $inputsList['page']['value'];
if (($limit = intval(env('ELEMENTS_LIMIT'))) < 5)
	$limit = 5;
$offset = ($numPage - 1) * $limit;

if (($nbrElements = db_count_posts()) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_count_posts()');
	return (false);
}
$nbrPages = get_number_pages($nbrElements);

if ($numPage > $nbrPages && $nbrPages != 0) {
	print_to_user('Warning', 'This page does not exist');
	exit();
}

if (($posts = db_get_all_posts_paginated($limit, $offset)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_get_all_posts_paginated()');
	return (false);
}
else if ($posts === ReturnCase::Empty) {
	$posts = null;
}

$DATA_FOR_VIEW = array('numPage' => $numPage,'nbrPages' => $nbrPages, 'posts' => $posts, 'title' => 'Public gallery', 'pageLink' => get_route_link('Public Gallery') .'?');

require_once($_SERVER['DOCUMENT_ROOT'] . '/gallery/gallery.view.php');
?>
