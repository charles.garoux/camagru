<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_pagination.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/pagination.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_user_profile.php');


/*
** Recois une requet HTTP avec en GET :
**	idUser (identifiant de l'utilisateur)
**	page	(numero de la page de la galerie)
**
** Lecture en BDD :
**	Utilisateur correspondant a l'idUser
**	Tous les post correspondant a l'idUser
**
** Si l'utilisateur n'existe pas :
**	Afficher un avertissement
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

$inputsList = [
	'idUser' => ['value' => ($_GET['idUser'] ?? NULL), 'type' => 'int'],
	'page' => ['value' => ($_GET['page'] ?? 1), 'type' => 'int']
];

if (check_input($inputsList, 'inputsList') === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$idUser = $inputsList['idUser']['value'];
$numPage = $inputsList['page']['value'];
if (($limit = intval(env('ELEMENTS_LIMIT'))) < 5)
	$limit = 5;
$offset = ($numPage - 1) * $limit;

if (($user = db_find_user_by_idUser($idUser)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_user_by_idUser()');
	return (false);
}
else if ($user === ReturnCase::Empty) {
	print_to_user('Warning', 'This user does not exist');
	exit();
}
unset($user['password']);
unset($user['email']);
unset($user['receiveEmail']);

if (($nbrElements = db_count_posts_by_idUser($idUser)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_count_posts()');
	return (false);
}
$nbrPages = get_number_pages($nbrElements);

if ($numPage > $nbrPages && $nbrPages != 0) {
	print_to_user('Warning', 'This page does not exist');
	exit();
}

if (($posts = db_find_posts_paginated_by_idUser($idUser, $limit, $offset)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_get_all_posts_paginated()');
	return (false);
}
else if ($posts === ReturnCase::Empty) {
	$posts = null;
}

$DATA_FOR_VIEW = array('numPage' => $numPage,'nbrPages' => $nbrPages, 'posts' => $posts, 'user' => $user, 'title' => 'Personnal gallery of '. $user['login'], 'pageLink' => get_route_link('User Gallery') .'?idUser='. $user['id'] . '&');

require_once($_SERVER['DOCUMENT_ROOT'] . '/gallery/gallery.view.php');
?>
