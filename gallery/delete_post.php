<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_posts.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_comments.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_likes.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');

/*
** Recois une requet HTTP avec en GET :
**	idPost (identifiant du post)
**
** Supprime en BDD :
**	Commentaire correspondant l'identifiant
**	Likes correspondant l'identifiant
**
** Si l'utilisateur n'est pas le proprietaire du post :
**	Afficher un avertissement
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (Auth::is_authenticate() === false) {
	print_to_user('Warning', 'To delete this post, you must be authenticated');
	exit();
}

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

$inputsList = [
	'idPost' => ['value' => ($_GET['idPost'] ?? NULL), 'type' => 'int']
];

if (check_input($inputsList, $_GET) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$idPost = $inputsList['idPost']['value'];

if (($post = db_find_post($idPost)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_post()');
	exit();
}
else if ($post === ReturnCase::Empty) {
	print_to_user('Warning', 'No post to delete');
	exit();
}

if ($_SESSION['user']['id'] != $post['idUser']) {
	print_to_user('Warning', 'Only the owner can delete a post');
	exit();
}

if (db_delete_likes_by_idPost($idPost) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_comments_by_idPost()');
	exit();
}

if (db_delete_comments_by_idPost($idPost) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_comments_by_idPost()');
	exit();
}

if (db_delete_post($idPost) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_post()');
	exit();
}

if ($_SERVER['HTTP_REFERER'] == get_route_link('Editing room').'/' || $_SERVER['HTTP_REFERER'] == get_route_link('Editing room'). '/index.php')
	$redirect = null;
else
	$redirect = get_route_link('Gallery');

print_flash_page('Success', 'The post has been deleted', $redirect);
?>
