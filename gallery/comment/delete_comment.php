<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_comments.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');

/*
** Recois une requet HTTP avec en GET :
**	idComment (identifiant du commentaire)
**
** Supprime en BDD :
**	Commentaire correspondant l'identifiant
**
** Si l'utilisateur n'est pas le proprietaire du commentaire :
**	Afficher un avertissement
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (Auth::is_authenticate() === false) {
	print_to_user('Warning', 'To delete this comment you need to be authenticated');
	exit();
}

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

$inputsList = [
	'idComment' => ['value' => ($_GET['idComment'] ?? NULL), 'type' => 'int']
];

if (check_input($inputsList, $_GET) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$idComment = $inputsList['idComment']['value'];

if (($comment = db_find_comment($idComment)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_comment()');
	exit();
}
else if ($comment === ReturnCase::Empty) {
	print_to_user('Warning', 'No comments to delete');
	exit();
}

if ($_SESSION['user']['id'] != $comment['idUser']) {
	print_to_user('Warning', 'Only the owner can delete a comment');
	exit();
}

if (db_delete_comment($idComment) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_comment()');
	exit();
}

print_flash_page('Success', 'The comment has been deleted');
?>
