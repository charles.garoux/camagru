<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_comments.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_posts.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/notification.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_user_profile.php');

/*
** Recois une requet HTTP avec en POST :
**	idPost (identifiant du post)
**	text (texte du commentaire)
**
** Ajout en BDD :
**	Commentaire correspondant a l'utilisateur connecté et au post
** Lecture en BDD :
**	Post commenté
**	Utilisateur proprietaire du post
**
** Notifier par email (si il souhaite recevoir les email de notification)
** le propiétaire du post
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (Auth::is_authenticate() === false) {
	print_to_user('Warning', 'To comment on this content, you must be authenticated');
	exit();
}

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode POST');
	exit();
}

$inputsList = [
	'idPost' => ['value' => ($_POST['idPost'] ?? NULL), 'type' => 'int'],
	'text' => ['value' => ($_POST['text'] ?? NULL), 'type' => 'comment']
];

if (check_input($inputsList, $_POST) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$idPost = $inputsList['idPost']['value'];
$text = $inputsList['text']['value'];

if (db_add_comment($idPost, $_SESSION['user']['id'], $text) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_comment()');
	exit();
}

if (($post = db_find_post($idPost)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_post()');
	exit();
}

if ($_SESSION['user']['id'] != $post['idUser']) {
	if (($owner = db_find_user_by_idUser($post['idUser'])) === false) {
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_user_by_idUser()');
		exit();
	}

	if ($owner['receiveEmail'] == true) {
		$message = $_SESSION['user']['login'] .' commented on your post : '. env('APP_URL') .'/gallery/post.php?idPost='. $idPost;
		if (notify($owner['email'], $message) === false) {
			print_debug_mail(__FILE__, __LINE__, __FUNCTION__, 'notify()');
			exit();
		}
	}
}

print_flash_page('Success', 'The comment has been published', get_route_link('View Post') .'?idPost='. $idPost);
?>
