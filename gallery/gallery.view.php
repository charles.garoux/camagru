<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');

/*
** Vue d'une galerie
**
** Les informations de la galerie sont contenue dans l'array $DATA_FOR_VIEW
**
** (voir /gallery/public.php ou /gallery/user.php pour connaitre sont contenue)
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (isset($DATA_FOR_VIEW) === false) {
	print_debug_view(__FILE__, __LINE__, __FUNCTION__, 'manque de donnees pour le fonctionnement de la vue');
	exit();
}

/* ===================== TRAITEMENT DE LA VUE ==================== */

$postRouteLink = get_route_link('View Post') . '?idPost=';

$postsDatas = $DATA_FOR_VIEW['posts'];

$pageLink = $DATA_FOR_VIEW['pageLink'] . 'page=';

$numPage = $DATA_FOR_VIEW['numPage'];
$nbrPages = $DATA_FOR_VIEW['nbrPages'];


/*========== PREPARATION DES POSTS ==========*/

$postsList = '';
if (empty($postsDatas) == false) {
	foreach ($postsDatas as $post) {
		$imageSource = 'data:image/png;base64,'. $post['image'];
		$postLink = $postRouteLink . $post['id'];

		$postsList = $postsList . <<< POST
		<div class="col-xlg-2 col-lg-3 col-md-4 col-sm-6 col-12 my-3">
		<div class="card px-0 py-0">
		<a href="$postLink">
		<img src="$imageSource" class="card-img-top">
		</a>
		</div>
		</div>
POST;
	}
}

/*========== PREPARATION DE LA PAGINATION ==========*/

$pagesList = '';
if ($nbrPages > 1) {
	if ($numPage > 1) {
		$link = $pageLink . ($numPage - 1);

		$pagesList = $pagesList . <<< PREVIOUS_PAGE
		<li class="page-item"><a class="page-link" href="$link" tabindex="-1">Previous</a></li>
PREVIOUS_PAGE;
	}
	else {
		$pagesList = $pagesList . <<< PREVIOUS_PAGE
		<li class="page-item disabled"><a class="page-link" href="" tabindex="-1" aria-disabled="true">Previous</a></li>
PREVIOUS_PAGE;
	}

	for ($i = 1; $i <= $nbrPages; $i++) {
		$link = $pageLink . $i;

		$pagesList = $pagesList . <<< PAGE
		<li class="page-item"><a class="page-link" href="$link">$i</a></li>
PAGE;
	}

	if ($numPage < $nbrPages) {
		$link = $pageLink . ($numPage + 1);

		$pagesList = $pagesList . <<< NEXT_PAGE
		<li class="page-item"><a class="page-link" href="$link" tabindex="-1">Next</a></li>
NEXT_PAGE;
	}
	else {
		$pagesList = $pagesList . <<< NEXT_PAGE
		<li class="page-item disabled"><a class="page-link" href="" tabindex="-1" aria-disabled="true">Next</a></li>
NEXT_PAGE;
	}

}

$content = <<< CONTENT
<div class="row">
	$postsList
</div>
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    $pagesList
  </ul>
</nav>
CONTENT;

print_template($DATA_FOR_VIEW['title'], true, $content, $DATA_FOR_VIEW['title']);

?>
