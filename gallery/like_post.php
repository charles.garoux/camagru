<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_likes.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/flash_page.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_posts.php');

/*
** Recois une requet HTTP avec en GET :
**	idPost (identifiant du post)
**
** Ecriture ou suppression en BDD :
**	Like correspondant au post et a l'utilisateur connecté
**
** Si le post n'existe pas :
**	Afficher un avertissement
** Si le post n'est pas deja like :
**	Creer un like
** Sinon :
**	Supprimer le like
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (Auth::is_authenticate() === false) {
	print_to_user('Warning', 'To like/disliker this content you have to be authenticated');
	exit();
}

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

$inputsList = [
	'idPost' => ['value' => ($_GET['idPost'] ?? NULL), 'type' => 'int']
];

if (check_input($inputsList, $_GET) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$idPost = $inputsList['idPost']['value'];

if (db_find_post($idPost) === false) {
	print_to_user('Warning', 'This post does not exist');
	exit();
}

if (($like = db_find_like($_SESSION['user']['id'], $idPost)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_like()');
	exit();
}
else if ($like === ReturnCase::Empty) {
	if ((db_add_like(intval($_SESSION['user']['id']), $idPost)) === false) {
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_like()');
		exit();
	}
}
else {
	if (db_delete_like($like['id']) === false) {
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_like()');
		exit();
	}
}

print_flash_page('Success', 'Your action is register');
?>
