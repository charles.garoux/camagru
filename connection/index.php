<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');

/*
** Fomulaire de connexion
*/
$actionScript = get_route_link('Authentification Script');
$signUpLink = get_route_link('Sign Up');
$resetPasswordLink = get_route_link('Password Reset');

$content = <<< CONTENT
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg my-5 bg-tan rounded">
            <div class="card-header text-center">
                Sign In Datas
            </div>
            <div class="card-body">
                <form action="$actionScript" method="post">
                    <div class="form-group">
                        <label for="login"><strong>Login :</strong></label>
                        <input type="text" class="form-control" id="login" name="login" required>
                    </div>
                    <div class="form-group">
                        <label for="password"><strong>Password :</strong></label>
                        <input type="password" class="form-control" id="password" name="password" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Sign In</button>
                    <a class="btn btn-secondary float-right" href="$signUpLink" role="button">Sign Up</a>
                    <div class="row">
                        <div class="col-12">
                            <a class="btn btn-outline-warning btn-sm mt-3" href="$resetPasswordLink" role="button">Reset Password</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
CONTENT;

print_template('Sign In', false, $content);



















?>
