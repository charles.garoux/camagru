<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/flash_page.php');

/*
** Recois une requet HTTP GET
**
** Suppression en session :
**	user (information de l'utilisateur)
*/

Auth::sign_out();

print_flash_page('Success', 'You are disconnected, see you soon ;)', get_route_link('Gallery'));
?>
