<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/flash_page.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');


/*
** Recois une requet HTTP avec en POST :
**	login (identifiant de connection de l'utilisateur)
**	password (le mot de passe de l'utilisateur)
**
** Lecture en BDD :
**	Utilisateur correspondant au "login"
**
** Si il n'y a pas de ticket d'enregistrement en attente pour cet utilisateur et
** si le couple indentifiant/mot de passe existe.
**
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode POST');
	exit();
}

$inputsList = [
	'login' => ['value' => ($_POST['login'] ?? NULL), 'type' => 'login'],
	'password' => ['value' => ($_POST['password'] ?? NULL), 'type' => 'str']
];

if (check_input($inputsList, $_POST) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$login = $inputsList['login']['value'];
$password = $inputsList['password']['value'];

if (($res = Auth::sign_in($login, $password)) === false)
	exit();
else if ($res === ReturnCase::Empty)
	exit();

print_flash_page('Success', 'You are connected', get_route_link('Gallery'));
?>
