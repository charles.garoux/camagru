<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_utils.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/enum.php');

function db_add_user($login, $password, $email, $receiveEmail)
{
	global $DATABASE;

	$request = <<< REQUEST
INSERT INTO `User` (login, password, email, receiveEmail) VALUES (:login, :password, :email, :receiveEmail)
REQUEST;

	if (multi_prepare_exec_query($request, true,
			[['bind' => ':login', 'param' => $login, 'pdo_param_type' => PDO::PARAM_STR],
			['bind' => ':password', 'param' => $password, 'pdo_param_type' => PDO::PARAM_STR],
			['bind' => ':email', 'param' => $email, 'pdo_param_type' => PDO::PARAM_STR],
			['bind' => ':receiveEmail', 'param' => $receiveEmail, 'pdo_param_type' => PDO::PARAM_BOOL]]
		) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	// multi_prepare_exec_query($request, true, array(
	// 		array(':login', $login, PDO::PARAM_STR),
	// 		array(':password', $password, PDO::PARAM_STR),
	// 		array(':email', $email, PDO::PARAM_STR),
	// 		array(':receiveEmail', $receiveEmail, PDO::PARAM_BOOL)
	// ));

	return ($DATABASE->lastInsertId());
}

function db_add_registerTicket (int $idUser, string $key)
{
	global $DATABASE;

	$request = <<< REQUEST
INSERT INTO `RegisterTicket` (`key`, idUser) VALUES (:key, :idUser)
REQUEST;

	if (multi_prepare_exec_query($request, true,
			[['bind' => ':key', 'param' => $key, 'pdo_param_type' => PDO::PARAM_STR],
			['bind' => ':idUser', 'param' => $idUser, 'pdo_param_type' => PDO::PARAM_STR],
		]) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	return ($DATABASE->lastInsertId());
}

/*
** db_find_registerTicket:
** Retour special possible :
**	ReturnCase::Empty	(si aucun ticket est trouvé)
*/

function db_find_registerTicket($key)
{
	$request = <<< REQUEST
SELECT * FROM `RegisterTicket` WHERE `key` = :key
REQUEST;

	if (($res = prepare_exec_query($request, ':key', $key, PDO::PARAM_STR, true)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (check_query_result(get_first($res)));
}

/*
** db_delete_registerTicket:
** Delete register ticket by id.
*/

function db_delete_registerTicket($id)
{
	if (db_delete_element_by_id('RegisterTicket', $id) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_element_by_id()');
		return (false);
	}

	return (true);
}






/*
array('bind' => ':xxx', 'param' => xxx, 'pdo_param_type' => PDO::PARAM_)
*/


/****************************************************************************************
										Test Zone
****************************************************************************************/
return ; // Evite en cas d'include de lancer les tests.

function gen_key($longueur = 40)
    {
	    $listeCaractères = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $longueurListeCaractères = strlen($listeCaractères);
	    $clé = '';
	    for ($i = 0; $i < $longueur; $i++) {
	        $clé .= $listeCaractères[rand(0, $longueurListeCaractères - 1)];
	    }
	    return $clé;
	}

// echo PHP_EOL ."[1] db_add_user = ";
// if (($res = db_add_user('chgaroux', '$password', '$email', 0)) === false)
// 	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_user()');
// else
// 	var_dump($res);

echo PHP_EOL ."[2] db_add_user = ";
if (($res = db_add_user('tutu', '$password', '$email', 1)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_user()');
else
	var_dump($res);
//
// echo PHP_EOL ."[3] db_add_user = ";
// if (($res = db_add_user('titi', '$password', '$email', 101)) === false)
// 	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_user()');
// else
// 	var_dump($res);

$idUser = $res;
$key = gen_key();

echo PHP_EOL ."db_add_registerTicket = ";
if (($res = db_add_registerTicket($idUser, $key)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_registerTicket()');
else
	var_dump($res);

echo PHP_EOL ."db_find_registerTicket = ";
if (($res = db_find_registerTicket($key)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_registerTicket()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

// echo PHP_EOL ."db_delete_registerTicket = ";
// if (db_delete_registerTicket($res['id']))
// 	echo "Bien supprimé". PHP_EOL;
// else
// 	echo "Pas supprimé";
// if (($res = db_find_registerTicket($res['key'])) === false)
// 	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_registerTicket()');
// else if ($res == ReturnCase::Empty)
// 	echo "Suppresion verifier". PHP_EOL;
// else
// 	echo "Suppression non verifier". PHP_EOL;

?>
