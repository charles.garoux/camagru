<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/enum.php');

/*
** extract_query:
**
** Whenever a query is executed, fetch is result in a single array.
*/

function extract_query($res)
{
	$final_array = array();
	for ($row_no = 0; $row_no < $res->num_rows; $row_no++)
	{
		$res->data_seek($row_no);
		$final_array[] = $res->fetch_assoc();
	}
	return $final_array;
}

/*
** check_query_result:
**
** Check the result of a query and return a more detailed result if needed.
**
** $result : array
**
** Return case:
**	found or False => direct return
**	is empty => ReturnCase::empty
*/

function check_query_result($result)
{
	if (count($result) === 0 || $result == NULL)
		return (ReturnCase::Empty);
	if (ReturnCase::isValidValue($result) || $result !== false)
		return ($result);
	return (false);
}

/*
** get_first:
**
** Return the first element of the array, if it's an array.
*/

function get_first($res)
{
	return (is_array($res) ? $res[0] : $res);
}

/*
** prepare_exec_query:
**
** Prepare, bind and execute prepared query.
**
**	$request : object(PDOStatement)
**	$bind : string
**	$param
**	$pdo_param_type : const
**	$fetchData : bool
*/

function prepare_exec_query($request, $bind, $param, $pdo_param_type, $fetchData)
{
	global $DATABASE;

	if (!($request = $DATABASE->prepare($request))){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$DATABASE->prepare()');
		return (false);
	}
	if (!$request->bindParam($bind, $param, $pdo_param_type)){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->bindParam()');
		return (false);
	}

	if (!$request->execute()){
		$error = $request->errorInfo();
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->execute() => '. $error['2']);
		return (false);
	}
	if ($fetchData)
		$res = $request->fetchAll(PDO::FETCH_ASSOC);
	else
		$res = true;
	if (!$request->closeCursor()){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->closeCursor()');
		return (false);
	}

	return (check_query_result($res));
}

/*
** multi_prepare_exec_query:
**
** Multi-prepare, bind and execute prepared query.
**
**	$request : object(PDOStatement)
**	$fetchData : bool
**	array(
**		'bind' : string
**		'param'
**		'pdo_param_type' : const
**	)
** Exemple paterne :
multi_prepare_exec_query($request, true, array(
		array('bind' => ':xxx', 'param' => xxx, 'pdo_param_type' => PDO::PARAM_),
		array('bind' => ':yyy', 'param' => yyy, 'pdo_param_type' => PDO::PARAM_),
	));
*/


function multi_prepare_exec_query($request, $fetchData, $bindsList)
{
	global $DATABASE;

	if (!($request = $DATABASE->prepare($request))){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$DATABASE->prepare()');
		return (false);
	}

	foreach ($bindsList as $bind) {
		if (!$request->bindParam($bind['bind'], $bind['param'], $bind['pdo_param_type'])){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->bindParam()');
			return (false);
		}
	}

	if (!$request->execute()){
		$error = $request->errorInfo();
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->execute() => '. $error['2']);
		return (false);
	}
	if ($fetchData)
		$res = $request->fetchAll(PDO::FETCH_ASSOC);
	else
		$res = true;
	if (!$request->closeCursor()){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->closeCursor()');
		return (false);
	}

	return (check_query_result($res));
}

/*
** db_find_element_by_id:
**
** Research element by id depending of the table.
*/

function db_find_element_by_id($table, $id)
{
	$request = <<< REQUEST
SELECT * FROM `$table` WHERE id=:id
REQUEST;

	if (($res = prepare_exec_query($request, ':id', $id, PDO::PARAM_INT, true)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	$res = check_query_result($res);
	return (is_array($res) ? $res[0] : $res);
}

/*
** db_delete_element_by_id:
**
** Delete element by id depending of the table.
** return:
** - True if the element havse been deleted.
** - False if an error occur : SQL injection, connection errors etc.
*/

function db_delete_element_by_id($table, $id)
{
	global $DATABASE;

	$request = <<< REQUEST
DELETE FROM `$table` WHERE id=:id
REQUEST;

	if (($res = prepare_exec_query($request, ':id', $id, PDO::PARAM_INT, false)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (true);
}

/*
** db_find_element_by_id:
**
** Research the first element by idUser depending of the table.
*/

function db_find_element_by_idUser($table, $idUser)
{
	if (($res = db_find_elements_by_idUser($table, $idUser)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_elements_by_idUser');
		return (false);
	}

	$res = check_query_result($res);
	return (is_array($res) ? $res[0] : $res);
}

/*
** db_find_elements_by_idUser:
**
** Research elements by idUser depending of the table.
*/

function db_find_elements_by_idUser($table, $idUser)
{
	global $DATABASE;

	$request = <<< REQUEST
SELECT * FROM `$table` WHERE idUser=:idUser
REQUEST;

	if (($res = prepare_exec_query($request, ':idUser', $idUser, PDO::PARAM_INT, true)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (check_query_result($res));
}

/*
** db_delete_elements_by_idUser:
**
** Delete elements by idUser depending of the table.
** return:
** - True if the element havse been deleted.
** - False if an error occur : SQL injection, connection errors etc.
*/

function db_delete_elements_by_idUser($table, $idUser)
{
	$request = <<< REQUEST
DELETE FROM `$table` WHERE idUser=$idUser
REQUEST;

	if (($res = prepare_exec_query($request, ':idUser', $idUser, PDO::PARAM_INT, false)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (check_query_result($res));
}

/*
** db_find_all_elements:
**
** Research all elements depending of the table.
*/

function db_find_all_elements($table)
{
	global $DATABASE;

	$request = <<< REQUEST
SELECT * FROM `$table`
REQUEST;

	if (!($res = $DATABASE->query($request))){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->execute()');
		return (false);
	}

	$res = $res->fetchAll(PDO::FETCH_ASSOC);
	return (check_query_result($res));
}

/*
** db_count_elements:
**
** Count all elements depending of the table.
*/

function db_count_elements($table)
{
	global $DATABASE;

	$request = <<< REQUEST
SELECT COUNT(*) AS nb_elements FROM `$table`
REQUEST;

	if (!($res = $DATABASE->query($request))){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->execute()');
		return (false);
	}

	$res = $res->fetchAll(PDO::FETCH_ASSOC);
	return (intval($res[0]['nb_elements']));
}

/*
** db_find_elements_by_idPost:
**
** Research elements by idPost depending of the table.
*/

function db_find_elements_by_idPost($table, $idPost)
{
	global $DATABASE;

	$request = <<< REQUEST
SELECT * FROM `$table` WHERE idPost=:idPost
REQUEST;

	if (($res = prepare_exec_query($request, ':idPost', $idPost, PDO::PARAM_INT, true)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (check_query_result($res));
}

/*
** db_delete_elements_by_idPost:
**
** Delete elements by idPost depending of the table.
** return:
** - True if the element havse been deleted.
** - False if an error occur : SQL injection, connection errors etc.
*/

function db_delete_elements_by_idPost($table, $idPost)
{
	$request = <<< REQUEST
DELETE FROM `$table` WHERE idPost=$idPost
REQUEST;

	if (($res = prepare_exec_query($request, ':idPost', $idPost, PDO::PARAM_INT, false)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (check_query_result($res));
}

/****************************************************************************************
										Test Zone
****************************************************************************************/
return ; // Evite en cas d'include de lancer les tests.

$table = 'User';
$id = 4;

echo PHP_EOL ."db_find_element_by_id = ";
if (($res = db_find_element_by_id($table, $id)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_element_by_id()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

echo PHP_EOL ."db_delete_element_by_id = ";
if (db_delete_element_by_id($table, $id))
	echo "Bien supprimé". PHP_EOL;
else
	echo "Pas supprimé". PHP_EOL;
if (db_find_element_by_id($table, $id) == ReturnCase::Empty)
	echo "Suppresion verifier". PHP_EOL;
else
	echo "Suppression non verifier". PHP_EOL;

$table = 'Post';
$idUser = 1;

echo PHP_EOL ."db_find_element_by_idUser = ";
if (($res = db_find_element_by_idUser($table, $idUser)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_element_by_idUser()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);


echo PHP_EOL ."db_find_elements_by_idUser = ";
if (($res = db_find_elements_by_idUser($table, $idUser)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_element_by_idUser()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

echo "\ndb_delete_elements_by_idUser = ";
if (db_delete_elements_by_idUser($table, $idUser))
	echo "Bien supprimé". PHP_EOL;
else
	echo "Pas supprimé";
if (($res = db_find_elements_by_idUser($table, $idUser)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->execute()');
else if ($res == ReturnCase::Empty)
	echo "Suppresion verifier". PHP_EOL;
else
	echo "Suppression non verifier". PHP_EOL;

$table = 'User';

echo PHP_EOL ."db_find_all_elements = ";
if (($res = db_find_all_elements($table)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_element_by_idUser()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

$table = 'Post';

echo PHP_EOL ."db_count_elements = ";
if (($res = db_count_elements($table)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_element_by_idUser()');
else
	var_dump($res);

?>
