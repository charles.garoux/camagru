<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/config/database.php');

/*
**	Utilise le fichier de configuration /config/database.php qui lui meme
**  utilise les variables d'environnement pour se connecter. Voir le fichier : .env
*/

try {
	$DATABASE = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
}
catch ( PDOException $Exception ) {
	print_debug('ERREUR', 'Message : '. $Exception->getMessage() .' | PDO dsn : '. $Exception->getTrace()[0]['args'][0]);
	exit();
}
?>
