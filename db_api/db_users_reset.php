<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_utils.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/enum.php');

function db_add_resetTicket (int $idUser, string $key)
{
	global $DATABASE;

	$request = <<< REQUEST
INSERT INTO `ResetTicket` (`key`, idUser) VALUES (:key, :idUser)
REQUEST;

	if (multi_prepare_exec_query($request, true,
			[['bind' => ':key', 'param' => $key, 'pdo_param_type' => PDO::PARAM_STR],
			['bind' => ':idUser', 'param' => $idUser, 'pdo_param_type' => PDO::PARAM_STR],
		]) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	return ($DATABASE->lastInsertId());
}

/*
** db_find_resetTicket:
** Retour special possible :
**	ReturnCase::Empty	(si aucun ticket est trouvé)
*/

function db_find_resetTicket($key)
{
	$request = <<< REQUEST
SELECT * FROM `ResetTicket` WHERE `key` = :key
REQUEST;

	if (($res = prepare_exec_query($request, ':key', $key, PDO::PARAM_STR, true)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (check_query_result(get_first($res)));
}

/*
** db_delete_resetTicket:
** Delete reset ticket by id.
*/

function db_delete_resetTicket($id)
{
	if (db_delete_element_by_id('ResetTicket', $id) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_element_by_id()');
		return (false);
	}

	return (true);
}

/****************************************************************************************
										Test Zone
****************************************************************************************/
return ; // Evite en cas d'include de lancer les tests.

function gen_key($longueur = 40)
    {
	    $listeCaractères = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $longueurListeCaractères = strlen($listeCaractères);
	    $clé = '';
	    for ($i = 0; $i < $longueur; $i++) {
	        $clé .= $listeCaractères[rand(0, $longueurListeCaractères - 1)];
	    }
	    return $clé;
	}


$idUser = 1;
$key = gen_key();

echo PHP_EOL ."db_add_resetTicket = ";
if (($res = db_add_resetTicket($idUser, $key)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_resetTicket()');
else
	var_dump($res);

echo PHP_EOL ."db_find_resetTicket = ";
if (($res = db_find_resetTicket($key)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_resetTicket()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

// echo PHP_EOL ."db_delete_resetTicket = ";
// if (db_delete_resetTicket($res['id']))
// 	echo "Bien supprimé". PHP_EOL;
// else
// 	echo "Pas supprimé";
// if (($res = db_find_resetTicket($res['key'])) === false)
// 	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_resetTicket()');
// else if ($res == ReturnCase::Empty)
// 	echo "Suppresion verifier". PHP_EOL;
// else
// 	echo "Suppression non verifier". PHP_EOL;

?>
