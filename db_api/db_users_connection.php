<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_utils.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/enum.php');

/*
** db_find_user:
** Retour special possible :
**	ReturnCase::Empty	(si aucun ticket est trouvé)
*/

function db_find_user($login)
{
	$request = <<< REQUEST
SELECT * FROM `User` WHERE `login` = :login
REQUEST;

	if (($res = prepare_exec_query($request, ':login', $login, PDO::PARAM_STR, true)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (check_query_result(get_first($res)));
}

function db_find_registerTicket_by_idUser($idUser)
{
	$request = <<< REQUEST
SELECT * FROM `RegisterTicket` WHERE `idUser` = :idUser
REQUEST;

	if (($res = prepare_exec_query($request, ':idUser', $idUser, PDO::PARAM_INT, true)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (check_query_result(get_first($res)));
}

/****************************************************************************************
										Test Zone
****************************************************************************************/
return ; // Evite en cas d'include de lancer les tests.

$login = 'chgaroux';

echo PHP_EOL ."db_find_user = ";
if (($res = db_find_user($login)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_user()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

echo PHP_EOL ."db_find_registerTicket_by_idUser = ";
if (($res = db_find_registerTicket_by_idUser($res['id'])) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_registerTicket_by_idUser()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);


?>
