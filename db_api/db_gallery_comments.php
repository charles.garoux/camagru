<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_utils.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/enum.php');

function db_add_comment($idPost, $idUser, $text)
{
	global $DATABASE;

	$request = <<< REQUEST
INSERT INTO `Comment` (idPost, idUser, text) VALUES (:idPost, :idUser, :text)
REQUEST;

	if (multi_prepare_exec_query($request, true,
			[['bind' => ':idPost', 'param' => $idPost, 'pdo_param_type' => PDO::PARAM_INT],
			['bind' => ':idUser', 'param' => $idUser, 'pdo_param_type' => PDO::PARAM_INT],
			['bind' => ':text', 'param' => $text, 'pdo_param_type' => PDO::PARAM_STR]
		]) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	return ($DATABASE->lastInsertId());
}

function db_delete_comment($idComment)
{
	if (($res = db_delete_element_by_id('Comment', $idComment)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_elements_by_idPost()');
		return (false);
	}

	return (true);
}

function db_update_comment($idComment, $text)
{

	$request = <<< REQUEST
UPDATE `Comment` SET text = :text WHERE id = :idComment
REQUEST;

	if (multi_prepare_exec_query($request, true,
			[['bind' => ':idComment', 'param' => $idComment, 'pdo_param_type' => PDO::PARAM_INT],
			['bind' => ':text', 'param' => $text, 'pdo_param_type' => PDO::PARAM_STR]
		]) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	return (true);
}

function db_find_all_comments_by_idPost($idPost)
{
	if (($res = db_find_elements_by_idPost('Comment', $idPost)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_elements_by_idPost()');
		return (false);
	}

	return ($res);
}

function db_find_comment($idComment)
{
	if (($res = db_find_element_by_id('Comment', $idComment)) === false) {
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_element_by_id()');
		return (false);
	}

	return ($res);
}

function db_delete_comments_by_idPost($idPost)
{
	if (($res = db_delete_elements_by_idPost('Comment', $idPost)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_elements_by_idPost()');
		return (false);
	}

	return ($res);
}

/****************************************************************************************
										Test Zone
****************************************************************************************/
return ; // Evite en cas d'include de lancer les tests.

$idPost = 1;

echo PHP_EOL ."db_add_comment = ";
if (($res = db_add_comment(1, 1, 'Ceci est un commentaire de teste.')) === false)
print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_comment()');
else
var_dump($res);

echo PHP_EOL ."db_delete_comment = ";
if (db_delete_comment($res))
	echo "Bien supprimé". PHP_EOL;
else
	echo "Pas supprimé";
if (($res = db_find_element_by_id('Comment', $res)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_comment()');
else if ($res == ReturnCase::Empty)
	echo "Suppresion verifier". PHP_EOL;
else
	echo "Suppression non verifier". PHP_EOL;

echo PHP_EOL ."db_find_all_comments_by_idPost = ";
if (($res = db_find_all_comments_by_idPost($idPost)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_all_comments_by_idPost()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

echo PHP_EOL ."db_update_comment = ";
if (db_update_comment($res[0]['id'], "Mise a jour". date("d-m-Y H:i:s")))
	echo "Bien mis a jour". PHP_EOL;
else
	echo "Pas mis a jour";
var_dump(db_find_element_by_id('Comment', $res[0]['id']));


?>
