<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_utils.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/enum.php');

function db_find_user_by_idUser($idUser)
{
	if (($res = db_find_element_by_id('User', $idUser)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_elements_by_id()');
		return (false);
	}

	return (check_query_result($res));
}

function db_update_user($idUser, $login, $password, $email, $receiveEmail)
{
	$request = <<< REQUEST
UPDATE `User` SET login = :login, password = :password, email = :email, receiveEmail = :receiveEmail WHERE id = :idUser
REQUEST;

	if (multi_prepare_exec_query($request, false, [
			['bind' => ':idUser', 'param' => $idUser, 'pdo_param_type' => PDO::PARAM_INT],
			['bind' => ':login', 'param' => $login, 'pdo_param_type' => PDO::PARAM_STR],
			['bind' => ':password', 'param' => $password, 'pdo_param_type' => PDO::PARAM_STR],
			['bind' => ':email', 'param' => $email, 'pdo_param_type' => PDO::PARAM_STR],
			['bind' => ':receiveEmail', 'param' => $receiveEmail, 'pdo_param_type' => PDO::PARAM_BOOL]]
		) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	return (true);
}


/****************************************************************************************
										Test Zone
****************************************************************************************/
return ; // Evite en cas d'include de lancer les tests.

$idUser = 2;

echo PHP_EOL ."db_find_user_by_idUser = ";
if (($res = db_find_user_by_idUser($idUser)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_user_by_idUser()');
else
	var_dump($res);

echo PHP_EOL ."db_update_user = ";
if (($res = db_update_user($idUser, $res['login'], $res['password'], 'email test '. rand(0, 101), true)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_update_user()');
else
	var_dump($res);

if (($res = db_find_user_by_idUser($idUser)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_user_by_idUser()');
else
	var_dump($res);
?>
