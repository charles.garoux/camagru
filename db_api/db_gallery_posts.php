<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_utils.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/enum.php');

function db_get_all_posts_by_date()
{
	global $DATABASE;

	$request = <<< REQUEST
SELECT * FROM `Post` ORDER BY creationDate
REQUEST;

	if (!($res = $DATABASE->query($request))){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$DATABASE->query()');
		return (false);
	}

	$res = $res->fetchAll(PDO::FETCH_ASSOC);
	return (check_query_result($res));
}

function db_find_posts_by_idUser_by_date($idUser)
{
	$request = <<< REQUEST
SELECT * FROM `Post` WHERE idUser = :idUser ORDER BY creationDate
REQUEST;

	if (($res = prepare_exec_query($request, ':idUser', $idUser, PDO::PARAM_INT, true)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (check_query_result($res));
}

function db_add_post($image, $idUser)
{
	global $DATABASE;

	$request = <<< REQUEST
INSERT INTO `Post` (image, idUser) VALUES (:image, :idUser)
REQUEST;


	if (!($request = $DATABASE->prepare($request))){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$DATABASE->prepare()');
		return (false);
	}
	if (!$request->bindParam(':image', $image, PDO::PARAM_LOB)){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->bindParam()');
		return (false);
	}
	if (!$request->bindParam(':idUser', $idUser, PDO::PARAM_INT)){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->bindParam()');
		return (false);
	}
	if (!$request->execute()){
		$error = $request->errorInfo();
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->execute() => '. $error['2']);
		return (false);
	}
	if (!$request->closeCursor()){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, '$request->closeCursor()');
		return (false);
	}

	return ($DATABASE->lastInsertId());
}

function db_delete_post($idPost)
{
	if (($res = db_delete_element_by_id('Post', $idPost)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_element_by_id()');
		return (false);
	}
	return (true);
}

function db_find_post($idPost)
{
	if (($res = db_find_element_by_id('Post', $idPost)) === false) {
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_element_by_id()');
		return (false);
	}

	return ($res);
}

/****************************************************************************************
										Test Zone
****************************************************************************************/
return ; // Evite en cas d'include de lancer les tests.

echo PHP_EOL ."db_get_all_posts_by_date = ";
if (($res = db_get_all_posts_by_date()) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_get_all_posts_by_date()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

$idUser = 4;

echo PHP_EOL ."db_find_posts_by_idUser_by_date = ";
if (($res = db_find_posts_by_idUser_by_date($idUser)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_posts_by_idUser_by_date()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

$image = "test db_add_post". date("d-m-Y H:i:s");
$idUser = 1;

echo PHP_EOL ."db_add_post = ";
if (($idPost = db_add_post($image, $idUser)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_posts_by_idUser_by_date()');
else
	var_dump($idPost);

// echo PHP_EOL ."db_delete_post = ";
// if (($res = db_delete_post($idPost)) === false)
// 	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_post()');
// else
// 	var_dump($res);

$idPost = 5;

echo PHP_EOL ."db_find_post = ";
if (($res = db_find_post($idPost)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_post()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

?>
