<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_utils.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/enum.php');

function db_get_all_posts_paginated ($limit, $offset)
{
	$request = <<< REQUEST
SELECT * FROM `Post` ORDER BY creationDate DESC LIMIT :limit OFFSET :offset
REQUEST;

	if (($res = multi_prepare_exec_query($request, true, [
			['bind' => ':limit', 'param' => $limit, 'pdo_param_type' => PDO::PARAM_INT],
			['bind' => ':offset', 'param' => $offset, 'pdo_param_type' => PDO::PARAM_INT]]
		)) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	return (check_query_result($res));
}

function db_find_posts_paginated_by_idUser($idUser, $limit, $offset)
{
	$request = <<< REQUEST
SELECT * FROM `Post` WHERE idUser = :idUser ORDER BY creationDate DESC LIMIT :limit OFFSET :offset
REQUEST;

	if (($res = multi_prepare_exec_query($request, true, [
			['bind' => ':idUser', 'param' => $idUser, 'pdo_param_type' => PDO::PARAM_INT],
			['bind' => ':limit', 'param' => $limit, 'pdo_param_type' => PDO::PARAM_INT],
			['bind' => ':offset', 'param' => $offset, 'pdo_param_type' => PDO::PARAM_INT]
			])) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	return (check_query_result($res));
}


function db_count_posts()
{
	if (($res = db_count_elements('Post')) === false) {
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
		return (false);
	}

	return ($res);
}

function db_count_posts_by_idUser($idUser)
{
	global $DATABASE;

	$request = <<< REQUEST
SELECT COUNT(*) AS nb_elements FROM `Post` WHERE idUser = :idUser
REQUEST;

	if (($res = prepare_exec_query($request, ':idUser', $idUser, PDO::PARAM_INT, true)) === false) {
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (intval($res[0]['nb_elements']));
}

/****************************************************************************************
										Test Zone
****************************************************************************************/
return ; // Evite en cas d'include de lancer les tests.

$limit = 5;
$offset = 10;

echo PHP_EOL ."db_get_all_posts_paginated = ";
if (($res = db_get_all_posts_paginated($limit, $offset)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_get_all_posts_paginated()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

$idUser = 1;

echo PHP_EOL ."db_find_posts_paginated_by_idUser = ";
if (($res = db_find_posts_paginated_by_idUser($idUser, $limit, $offset)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_posts_paginated_by_idUser()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

echo PHP_EOL ."db_count_posts = ";
if (($res = db_count_posts()) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_count_posts()');
else
	var_dump($res);

echo PHP_EOL ."db_count_posts_by_idUser = ";
if (($res = db_count_posts_by_idUser($idUser)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_count_posts_by_idUser()');
else
	var_dump($res);

?>
