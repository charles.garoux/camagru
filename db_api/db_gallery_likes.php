<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_utils.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/enum.php');

function db_add_like($idUser, $idPost)
{
	global $DATABASE;

	$request = <<< REQUEST
INSERT INTO `Like` (idPost, idUser) VALUES (:idPost, :idUser)
REQUEST;

	if (multi_prepare_exec_query($request, true,
			[['bind' => ':idPost', 'param' => $idPost, 'pdo_param_type' => PDO::PARAM_INT],
			['bind' => ':idUser', 'param' => $idUser, 'pdo_param_type' => PDO::PARAM_INT],
		]) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	return ($DATABASE->lastInsertId());
}

function db_delete_like($idLike)
{
	if (($res = db_delete_element_by_id('Like', $idLike)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_element_by_id()');
		return (false);
	}
	return (true);
}

function db_find_all_like_by_idPost($idPost)
{

	if (($res = db_find_elements_by_idPost('Like', $idPost)) === false) {
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_elements_by_idPost()');
		return (false);
	}

	return ($res);
}

function db_find_like($idUser, $idPost)
{
	$request = <<< REQUEST
SELECT * FROM `Like` WHERE idPost = :idPost AND idUser = :idUser
REQUEST;

	if (($res =multi_prepare_exec_query($request, true,
			[['bind' => ':idPost', 'param' => $idPost, 'pdo_param_type' => PDO::PARAM_INT],
			['bind' => ':idUser', 'param' => $idUser, 'pdo_param_type' => PDO::PARAM_INT],
		])) === false){
			print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'multi_prepare_exec_query()');
			return (false);
		}

	return (check_query_result(get_first($res)));
}

function db_count_likes($idPost)
{
	$request = <<< REQUEST
SELECT COUNT(*) AS nb_likes FROM `Like` WHERE idPost = :idPost
REQUEST;

	if (($res = prepare_exec_query($request, ':idPost', $idPost, PDO::PARAM_INT, true)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'prepare_exec_query()');
		return (false);
	}

	return (intval($res[0]['nb_likes']));
}

function db_delete_likes_by_idPost($idPost)
{
	if (($res = db_delete_elements_by_idPost('Like', $idPost)) === false){
		print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_elements_by_idPost()');
		return (false);
	}

	return ($res);
}

/****************************************************************************************
										Test Zone
****************************************************************************************/
return ; // Evite en cas d'include de lancer les tests.

echo PHP_EOL ."db_add_like = ";
if (($res = db_add_like(1, 1)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_like()');
else
	var_dump($res);

echo PHP_EOL ."db_delete_like = ";
if (db_delete_like($res))
	echo "Bien supprimé". PHP_EOL;
else
	echo "Pas supprimé";
if (($res = db_find_like(1, 1)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_like()');
else if ($res == ReturnCase::Empty)
	echo "Suppresion verifier". PHP_EOL;
else
	echo "Suppression non verifier". PHP_EOL;

$idPost = 1;

echo PHP_EOL ."db_find_all_like_by_idPost = ";
if (($res = db_find_all_like_by_idPost($idPost)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_all_like_by_idPost()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);


echo PHP_EOL ."db_count_likes = ";
if (($res = db_count_likes($idPost)) === false)
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_count_likes()');
else if ($res == ReturnCase::Empty)
	echo "ReturnCase::Empty". PHP_EOL;
else
	var_dump($res);

?>
