<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');

/*
** La liste de route principale de l'application
**
** Particularite : les bouttons seront affiche seulement si "Auth::is_authenticate()" correspond a "['need_auth']"
*/

if (isset($_SESSION['user']))
	$idUser = '?idUser=' . $_SESSION['user']['id'];

$ROUTES_LIST = array(
	/*===== PAGE DE LA BARRE DE NAVIGATION =====*/
	'Home' => ['link' => '/gallery/public.php', 'nav_bar' => false, 'need_auth' => false],
	'Gallery' => ['link' => '/gallery/public.php', 'nav_bar' => true, 'need_auth' => false],
	'My gallery' => ['link' => '/gallery/user.php' . ($idUser ?? ''), 'nav_bar' => true, 'need_auth' => true],
	'Editing room' => ['link' => '/editing_room', 'nav_bar' => true, 'need_auth' => true],
	/*===== BOUTON DE LA BARRE DE NAVIGATION =====*/
	'My Profile' => ['link' => '/user_profile/profile_data.php', 'nav_bar' => 'button', 'need_auth' => true],
	'Sign In' => ['link' => '/connection', 'nav_bar' => 'button', 'need_auth' => false],
	'Sign Up' => ['link' => '/registration', 'nav_bar' => 'button', 'need_auth' => false],
	'Sign Out' => ['link' => '/connection/disconnection.php', 'nav_bar' => 'button', 'need_auth' => true],
	/*========== AUTRE ==========*/
		/*========== GESTION DE COMPTE ==========*/
	'Password Reset' => ['link' => '/password_reset', 'nav_bar' => false, 'need_auth' => false],
	'Password Reset Request' => ['link' => '/password_reset/request.php', 'nav_bar' => false, 'need_auth' => false],
	'Password Reset Update Form' => ['link' => '/password_reset/new_password.form.php', 'nav_bar' => false, 'need_auth' => false],
	'Password Reset Update' => ['link' => '/password_reset/update.php', 'nav_bar' => false, 'need_auth' => false],
	'Authentification Script' => ['link' => '/connection/authentification.php', 'nav_bar' => false, 'need_auth' => false],
	'New Account Script' => ['link' => '/registration/new_account.php', 'nav_bar' => false, 'need_auth' => false],
	'My Profile Update' => ['link' => '/user_profile/update_user_data.php', 'nav_bar' => false, 'need_auth' => false],
		/*========== GALERIE ==========*/
	'View Post' => ['link' => '/gallery/post.php', 'nav_bar' => false, 'need_auth' => false],
	'Like Post' => ['link' => '/gallery/like_post.php', 'nav_bar' => false, 'need_auth' => true],
	'Delete Post' => ['link' => '/gallery/delete_post.php', 'nav_bar' => false, 'need_auth' => true],

	'Add Comment' => ['link' => '/gallery/comment/add_comment.php', 'nav_bar' => false, 'need_auth' => true],
	'Delete Comment' => ['link' => '/gallery/comment/delete_comment.php', 'nav_bar' => false, 'need_auth' => true],

	'User Gallery' => ['link' => '/gallery/user.php', 'nav_bar' => false, 'need_auth' => false],
	'Public Gallery' => ['link' => '/gallery/public.php', 'nav_bar' => false, 'need_auth' => false],
	/*========== SALLE DE MONTAGE ==========*/
	'Editing Script' => ['link' => '/editing_room/merge.php', 'nav_bar' => false, 'need_auth' => true],
);

if(!function_exists('get_route')) {

  function get_route($routeName)
  {
		global $ROUTES_LIST;

		if (array_key_exists($routeName, $ROUTES_LIST)) {
			return ($ROUTES_LIST[$routeName]);
		}
		print_debug_routing(__FILE__, __LINE__, __FUNCTION__, 'la route "'. $routeName .'" n\'existe pas');
		return (false);
  }
}

if(!function_exists('get_route_link')) {

  function get_route_link($routeName)
  {
		if (($route = get_route($routeName)) === false) {
			print_debug_routing(__FILE__, __LINE__, __FUNCTION__, 'get_route()');
			return (false);
		}

		return (env('APP_URL'). $route['link']);
  }
}




?>
