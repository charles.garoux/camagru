<?php
if(!function_exists('env')) {
  $env_file = $_SERVER['DOCUMENT_ROOT'] ."/.env";

  if (file_exists($env_file) === False)
  {
    echo "[ERREUR] ".__FILE__." :Le fichier d'environnement \"$env_file\" n'existe pas" . PHP_EOL;
    exit(1);
  }

  $env_content = file($env_file);

  foreach ($env_content as $key => $value) {
    $var = trim($value);
    if ($var != NULL)
      putenv($var);
  }

  function env($key, $default = null)
  {
    $value = getenv($key);
    if ($value === false) {
			if ($default == null)
				echo "[ERREUR] ".__FUNCTION__."() : La clé $key n'existe pas dans l'environnement" .PHP_EOL;
      return ($default);
    }
    return ($value);
  }
}
?>
