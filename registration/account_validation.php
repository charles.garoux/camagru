<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_register.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');

/*
** Recois une requet HTTP avec en GET :
**	key (clé du ticket d'enregistrement)
**
** Suppression en BDD :
**	ticket d'enregistrement correspondant
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'GET'){
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

$inputsList = [
	'key' => ['value' => ($_GET['key'] ?? NULL), 'type' => 'str']
];

if (check_input($inputsList, $_GET) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$key = $inputsList['key']['value'];

if (($registerTicket = db_find_registerTicket($key)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_registerTicket()');
	exit();
}
else if ($registerTicket == ReturnCase::Empty){
	print_to_user('Warning', 'The key '. $key .' does not exist.');
	exit();
}

if (($registerTicket = db_delete_registerTicket($registerTicket['id'])) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_registerTicket()');
	exit();
}

print_flash_page('Success', 'Your account is validate', get_route_link('Sign In'));
?>
