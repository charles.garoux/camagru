<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');

/*
** Fomulaire d'inscription
**
** La verification de force du mot de passe peut etre active/desactive
** avec la variable d'environnement CHECK_PASSWORD_STRENGTH
*/

$actionScript = get_route_link('New Account Script');

if (env('CHECK_PASSWORD_STRENGTH') === 'TRUE') {
	$script = <<< SCRIPT

	let passwordForm = document.getElementById("password");
	let password_strength = document.getElementById("password_strength");
	document.getElementById("submit").disabled = false;

	passwordForm.oninput = CheckPasswordStrength;

	function CheckPasswordStrength(e) {
		var password = e.target.value;

		//if textBox is empty
		if(password.length == 0){
			password_strength.innerHTML = "";
			return;
		}

		//Regular Expressions
		var regex = new Array();
		regex.push("[A-Z]"); //For Uppercase Alphabet
		regex.push("[a-z]"); //For Lowercase Alphabet
		regex.push("[0-9]"); //For Numeric Digits
		regex.push("[$@$!%*#?&]"); //For Special Characters

		var score = 0;

		//Validation for each Regular Expression
		for (var i = 0; i < regex.length; i++) {
			if((new RegExp (regex[i])).test(password)){
				score++;
			}
		}

		//Validation for Length of Password
		if(score > 2 && password.length > 8){
			score++;
		}

		//Display of Status
		var color = "";
		var passwordStrengthText = "";
		var canSubmit = false;
		switch(score){
			case 0:
			canSubmit = false;
			break;
			case 1:
			passwordStrengthText = "Password is Weak.";
			color = "Red";
			canSubmit = false;
			break;
			case 2:
			passwordStrengthText = "Password is Good.";
			color = "darkorange";
			canSubmit = false;
			break;
			case 3:
			passwordStrengthText = "Password is Good.";
			color = "Green";
			canSubmit = true;
			break;
			case 4:
			passwordStrengthText = "Password is Strong.";
			color = "Green";
			canSubmit = true;
			break;
			case 5:
			passwordStrengthText = "Password is Very Strong.";
			color = "darkgreen";
			canSubmit = true;
			break;
		}
		document.getElementById("submit").disabled = !canSubmit;
		password_strength.innerHTML = passwordStrengthText;
		password_strength.style.color = color;
	}
SCRIPT;
}
else {
	$script = <<< SCRIPT
	let password_strength = document.getElementById("password_strength");
	password_strength.innerHTML = "";
SCRIPT;
}

$content = <<< CONTENT
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg my-5 bg-tan rounded">
            <div class="card-header text-center">
                Sign Up Datas
            </div>
            <div class="card-body">
                <form action="$actionScript" method="post">
									<div class="form-group">
										<label for="login"><strong>Login :</strong></label>
										<input type="text" class="form-control" id="login" name="login" required>
									</div>
									<div class="form-group">
										<label for="password"><strong>Password :</strong></label> <span id="password_strength">Strength...</span>
										<input type="password" class="form-control" id="password" name="password" required>
									</div>
									<div class="form-group">
                      <label for="email"><strong>Email :</strong></label>
                      <input type="email" class="form-control" id="email" name="email" required>
                  </div>
									<div class="form-group">
										<label for="receiveEmail"><strong>Notification by email :</strong></label>
									    <select class="form-control" id="receiveEmail" name="receiveEmail">
									      <option value="true">I agree to receive notification by email</option>
										  <option value="false">I do not accept to receive email notifications</option>
									    </select>
									  </div>
                    <button type="submit" id="submit" class="btn btn-primary">Sign Up</button>
                </form>
            </div>
        </div>
    </div>
</div>
CONTENT;

print_template('Sign Up', false, $content, null, $script);



















?>
