<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_register.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/key_generator.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/send_mail.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');


/*
** Recois une requet HTTP avec en POST :
**	login (identifiant de connection de l'utilisateur)
**	password (le mot de passe de l'utilisateur)
**	email (l'email de l'utilisateur)
**	receiveEmail (booleen indiquant si l'utilisateur veux recevoir des notification par mail)
**
** Vérifier si l'identifiant de connection n'existe pas déjà
** Ajout en BDD :
**	l'utilisateur
**	un ticket d'enregistrement
** Envoie un email de confirmation
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode POST');
	exit();
}

$inputsList = [
	'login' => ['value' => ($_POST['login'] ?? NULL), 'type' => 'login'],
	'password' => ['value' => ($_POST['password'] ?? NULL), 'type' => 'str'],
	'email' => ['value' => ($_POST['email'] ?? NULL), 'type' => 'email'],
	'receiveEmail' => ['value' => ($_POST['receiveEmail'] ?? NULL), 'type' => 'bool']
];

if (check_input($inputsList, $_POST) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$login = $inputsList['login']['value'];
$password = $inputsList['password']['value'];
$email = $inputsList['email']['value'];
$receiveEmail = $inputsList['receiveEmail']['value'];

if (db_find_user($login) != ReturnCase::Empty) {
	print_to_user('Warning', 'The login "'. $login .'" already exists');
	exit();
}

$passwordHashed = hash(env('HASH_ALGO'), $password);

if (($idUser = db_add_user($login, $passwordHashed, $email, $receiveEmail)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_user()');
	exit();
}

$registerKey = key_generator();

if (db_add_registerTicket($idUser, $registerKey) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_registerTicket()');
	exit();
}

$to = $email;
$subject = 'Registration at Camagru';
$message = 'Welcome, '. PHP_EOL ."Validation link : ". env('APP_URL') .'/registration/account_validation.php?key='. $registerKey;

if (send_mail($to, $subject, $message) === false){
	print_debug('ERREUR', '[MAIL]'.__FILE__.':'.(__LINE__ - 1));
	exit();
}

print_flash_page('Success', 'You are register, check your email for validation', get_route_link('Gallery'), 20000);
?>
