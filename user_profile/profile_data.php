<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_user_profile.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');

/*
** Recois une requet HTTP GET
**
** Recherche en BDD :
**	toutes les information sur l'utilisateur correspondant a "id"
**	en session de l'utilisateur
*/

/* ==================== VÉRIFICATION STANDARD ==================== */
if (Auth::is_authenticate() === false) {
	print_to_user('Warning', 'To access this content, you must be authenticated');
	exit();
}

if ($_SERVER['REQUEST_METHOD'] !== 'GET'){
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

if (($user = db_find_user_by_idUser($_SESSION['user']['id'])) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_user_by_idUser()');
	exit();
}

$DATA_FOR_VIEW = $user;
unset($DATA_FOR_VIEW['id']);
$DATA_FOR_VIEW['password'] = '';

require_once($_SERVER['DOCUMENT_ROOT'] . '/user_profile/my_profile.view.php');
?>
