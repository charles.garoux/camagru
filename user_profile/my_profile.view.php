<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');

/*
** Vue d'une galerie
**
** Les informations de la galerie sont contenue dans l'array $DATA_FOR_VIEW
**
** (voir /gallery/public.php ou /gallery/user.php pour connaitre sont contenue)
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (isset($DATA_FOR_VIEW) === false) {
	print_debug_view(__FILE__, __LINE__, __FUNCTION__, 'manque de donnees pour le fonctionnement de la vue');
	exit();
}

/* ===================== TRAITEMENT DE LA VUE ==================== */
$actionScript = get_route_link('My Profile Update');

$login = $DATA_FOR_VIEW['login'];
$email = $DATA_FOR_VIEW['email'];
$receiveEmail = $DATA_FOR_VIEW['receiveEmail'];

/*========== PREPARATION DES FORMULAIRE ==========*/

$formList = '';
foreach ($DATA_FOR_VIEW as $key => $value) {
	if ($key == 'email' || $key == 'password') {
		$type = $key;
	}
	else {
		$type = 'text';
	}
	if ($key == 'receiveEmail') {
		if ($value == true) {
			$trueStatus = 'selected';
			$falseStatus = '';
		}
		else {
			$trueStatus = '';
			$falseStatus = 'selected';
		}

		$formList = $formList . <<< FORM
		<div class="form-group">
		<label for="$key"><strong>Notification by email :</strong></label>
			<select class="form-control" id="$key" name="$key">
				<option value="true" $trueStatus>I agree to receive notification by email</option>
				<option value="false" $falseStatus>I do not accept to receive email notifications</option>
			</select>
		</div>
FORM;
	}
	else if ($key == 'password') {
	$formList = $formList . <<< FORM
	<div class="form-group">
		<label for="$key"><strong>$key :</strong></label> <span id="password_strength">Strength...</span>
		<input type="$type" class="form-control" id="$key" name="$key">
	</div>
FORM;
	}
	else if ($key == 'login') {
	$formList = $formList . <<< FORM
	<div class="form-group">
		<label for="$key"><strong>$key :</strong></label>
		<input type="$type" class="form-control" id="$key" name="$key" value="$value" maxlength="32">
	</div>
FORM;
	}
	else {
		$formList = $formList . <<< FORM
		<div class="form-group">
		<label for="$key"><strong>$key :</strong></label>
		<input type="$type" class="form-control" id="$key" name='$key' value="$value">
		</div>
FORM;
	}

}

if (env('CHECK_PASSWORD_STRENGTH') === 'TRUE') {
	$script = <<< SCRIPT

	let passwordForm = document.getElementById("password");
	let password_strength = document.getElementById("password_strength");

	passwordForm.oninput = CheckPasswordStrength;

	function CheckPasswordStrength(e) {
		var password = e.target.value;

		//if textBox is empty
		if(password.length == 0){
			password_strength.innerHTML = "";
			return;
		}

		//Regular Expressions
		var regex = new Array();
		regex.push("[A-Z]"); //For Uppercase Alphabet
		regex.push("[a-z]"); //For Lowercase Alphabet
		regex.push("[0-9]"); //For Numeric Digits
		regex.push("[$@$!%*#?&]"); //For Special Characters

		var score = 0;

		//Validation for each Regular Expression
		for (var i = 0; i < regex.length; i++) {
			if((new RegExp (regex[i])).test(password)){
				score++;
			}
		}

		//Validation for Length of Password
		if(score > 2 && password.length > 8){
			score++;
		}

		//Display of Status
		var color = "";
		var passwordStrengthText = "";
		switch(score){
			case 0:
			break;
			case 1:
			passwordStrengthText = "Password is Weak.";
			color = "Red";
			break;
			case 2:
			passwordStrengthText = "Password is Good.";
			color = "darkorange";
			break;
			case 3:
			passwordStrengthText = "Password is Good.";
			color = "Green";
			break;
			case 4:
			passwordStrengthText = "Password is Strong.";
			color = "Green";
			break;
			case 5:
			passwordStrengthText = "Password is Very Strong.";
			color = "darkgreen";
			break;
		}
		password_strength.innerHTML = passwordStrengthText;
		password_strength.style.color = color;
	}
SCRIPT;
}
else {
	$script = <<< SCRIPT
	let password_strength = document.getElementById("password_strength");
	password_strength.innerHTML = "";
SCRIPT;
}




$content = <<< CONTENT
<div class="row">
	<form class="col-12" action="$actionScript" method="post">
		<div class="card bg-white px-3 my-3">
			<div class="card-body">
				$formList
				<button type="submit" class="btn btn-primary">Update profile</button>
			</div>
		</div>
	</form>
</div>

CONTENT;

print_template('My Profile', true, $content, 'My profile', $script);

?>
