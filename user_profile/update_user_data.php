<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_user_profile.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/flash_page.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_connection.php');

/*
** Recois une requet HTTP avec en POST :
**	login (identifiant nouveau de connection de l'utilisateur)
**	password (le nouveau mot de passe de l'utilisateur)
**	email (le nouveau email de l'utilisateur)
**	receiveEmail (le nouveau )
**
** Lecture en BDD :
**	Utilisateur correspondant au "login"
**
** Si il n'y a pas de ticket d'enregistrement en attente pour cet utilisateur et
** si le couple indentifiant/mot de passe existe.
**
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (Auth::is_authenticate() === false) {
	print_to_user('Warning', 'To access this content, you must be authenticated');
	exit();
}

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode POST');
	exit();
}

/* ======== RECUPERATION DE DONNEES POUR LA VERIFICATION ======== */
if (($origin_user_datas = db_find_user_by_idUser($_SESSION['user']['id'])) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_user_by_idUser()');
	exit();
}
/* ==================== SUITE VÉRIFICATION  ==================== */

if (empty($_POST['password']) === false)
	$inputPassword = hash(env('HASH_ALGO'), $_POST['password']);

$inputsList = [
	'login' => ['value' => ((!empty($_POST['login'])) ? $_POST['login'] : $origin_user_datas['login']), 'type' => 'login'],
	'password' => ['value' => ($inputPassword ?? $origin_user_datas['password']), 'type' => 'str'],
	'email' => ['value' => ((!empty($_POST['email'])) ? $_POST['email'] : $origin_user_datas['email']), 'type' => 'email'],
	'receiveEmail' => ['value' => ((!empty($_POST['receiveEmail'])) ? $_POST['receiveEmail'] : $origin_user_datas['receiveEmail']), 'type' => 'bool']
];

if (check_input($inputsList, 'inputsList') === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$idUser = $_SESSION['user']['id'];
$login = $inputsList['login']['value'];
$password = $inputsList['password']['value'];
$email = $inputsList['email']['value'];
$receiveEmail = $inputsList['receiveEmail']['value'];

if ($login != $_SESSION['user']['login'] && db_find_user($login) != ReturnCase::Empty) {
	print_to_user('Warning', 'The login "'. $login .'" already exists');
	exit();
}

if (($res = db_update_user($idUser, $login, $password, $email, $receiveEmail)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_update_user()');
	exit();
}
$_SESSION['user']['login'] = $login;

print_flash_page('Success', 'Your profile is updated', get_route_link('My Profile'));
?>
