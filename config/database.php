<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');

$DB_DSN = 'mysql:host='. env('DB_HOST') .';port=' . env('DB_PORT') .';dbname='. env('DB_DATABASE');
$DB_USER = env('DB_USERNAME');
$DB_PASSWORD = env('DB_PASSWORD');
?>
