<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_connect.php');

if (($script = @file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/config/init_DB.sql')) === false) {
	print_debug('ERREUR', '[FILESYSTEM]'.__FILE__.':'.(__LINE__ - 1) .' file_get_contents()');
	exit();
}

if ($DATABASE->query($script) === false) {
	print_debug_db(__FILE__, __LINE__, '', 'query()');
	exit();
}

?>
