<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');

/*
** Page de la camera avec montage photo
**
** Le montage finale est fait cote serveur en PHP (contrainte du sujet)
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (Auth::is_authenticate() === false) {
	print_to_user('Warning', 'To make a montage you have to be authenticated');
	exit();
}

/* ===================== TRAITEMENT DE LA VUE ==================== */

$actionScript = get_route_link('Editing Script');

/* ===================== CAMERA ==================== */

$cameraView = <<< CAMERA_VIEW
	<div id="camera">
		<video hidden id="video"></video>
		<canvas id="canvas" class="card-img-top" data-filter-name=""></canvas>
	</div>
	<div class="card-body">
		<button id="shot" class="btn btn-primary" disabled>Take photo</button>
  </div>

CAMERA_VIEW;

/* ===================== FILTRES ==================== */

$filtersList = '';
$files = glob('./filters/*.{png}', GLOB_BRACE);
$img_name_regex = '/.+\/(.+).png/';
foreach($files as $filePath) {
	preg_match($img_name_regex, $filePath, $img_name);

	$filtersList = $filtersList . <<< FILTER
	<div class="col-md-6 col-12">
		<div class="card px-0 py-0 mx-2 my-2">
			<input type="image" name="filter" data-filter-name="$img_name[1]" src="$filePath" class="filter card-img">
		</div>
	</div>
FILTER;
}

/* ===================== SCRIPT JS ==================== */

$scriptCamera = <<< SCRIPT_CAMERA
<script type="module" src="pic.js"></script>
SCRIPT_CAMERA;

/* ===================== CONTENUE DE LA VUE ==================== */

$content = <<< CONTENT
<div class="row">
	<div id="cameraBox" class="col-md-7 col-12 mt-4">
		<div class="card">
				$cameraView
		</div>
	</div>

	<div id="filtersList" class="col-md-5 col-12 mt-4">
		<div class="card">
				<h5 class="card-header">Filters</h5>
				<div class="card-body">
					<div class="row">
						$filtersList
					</div>
			  </div>
		</div>
	</div>
</div>
<form id="shotsList" class="row" action="$actionScript" method="post">

</form>
$scriptCamera
CONTENT;

print_template('Editing room', true, $content);
?>
