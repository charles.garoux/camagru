<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_pagination.php');

/*
** Page de la camera avec montage photo
**
** Le montage finale est fait cote serveur en PHP (contrainte du sujet)
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (Auth::is_authenticate() === false) {
	print_to_user('Warning', 'To make a montage you have to be authenticated');
	exit();
}

/* ==================== RECUPERATION DE DONNEES ==================== */

if (($posts = db_find_posts_paginated_by_idUser($_SESSION['user']['id'], 4, 0)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_get_all_posts_paginated()');
	return (false);
}
else if ($posts === ReturnCase::Empty) {
	$posts = null;
}


/* ===================== TRAITEMENT DE LA VUE ==================== */

$actionScript = get_route_link('Editing Script');
$postRouteLink = get_route_link('View Post') .'?idPost=';
$deletePostRouteLink = get_route_link('Delete Post') .'?idPost=';

/*========== PREPARATION DES POSTS ==========*/

$postsList = '';
if (empty($posts) == false) {
	foreach ($posts as $post) {
		$imageSource = 'data:image/png;base64,'. $post['image'];
		$postLink = $postRouteLink . $post['id'];
		$deletePostLink = $deletePostRouteLink . $post['id'];

		$postsList = $postsList . <<< POST
		<div class="col-xlg-2 col-lg-3 col-md-4 col-sm-6 col-12 my-3">
			<div class="card px-0 py-0">
				<a href="$postLink">
				<img src="$imageSource" class="card-img-top">
				</a>
				<div class="card-body">
		  	    <a href="$deletePostLink" class="btn btn-outline-danger btn-sm">Delete my post</a>
		  	  </div>
			</div>
		</div>
POST;
	}
}

/* ===================== CAMERA ==================== */

$cameraView = <<< CAMERA_VIEW
	<div id="camera">
		<video hidden id="video"></video>
		<canvas id="canvas" class="card-img-top" data-filter-name=""></canvas>
	</div>
	<div class="card-body">
		<button id="shot" class="btn btn-primary" disabled>Take photo</button>
  </div>

CAMERA_VIEW;

/* ===================== FILTRES ==================== */

$filtersList = '';
$files = glob('./filters/*.{png}', GLOB_BRACE);
$img_name_regex = '/.+\/(.+).png/';
foreach($files as $filePath) {
	preg_match($img_name_regex, $filePath, $img_name);

	$filtersList = $filtersList . <<< FILTER
	<div class="col-md-6 col-12">
		<div class="card px-0 py-0 mx-2 my-2">
			<input type="image" name="filter" data-filter-name="$img_name[1]" src="$filePath" class="filter card-img">
		</div>
	</div>
FILTER;
}

/* ===================== SCRIPT JS ==================== */

$scriptCamera = <<< SCRIPT_CAMERA
<script type="module" src="pic.js"></script>
SCRIPT_CAMERA;

/* ===================== CONTENUE DE LA VUE ==================== */

$content = <<< CONTENT
<div class="row">
	<div id="cameraBox" class="col-md-7 col-12 mt-4">
		<div class="card">
				$cameraView
		</div>
	</div>

	<div id="filtersList" class="col-md-5 col-12 mt-4">
		<div class="card">
				<h5 class="card-header">Filters</h5>
				<div class="card-body">
					<div class="row">
						$filtersList
					</div>
			  </div>
		</div>
	</div>
</div>
<form id="imageShoot" action="$actionScript" method="post">
	<input type="hidden" id="filterInput" name="filterName" value="">
	<input type="hidden" id="imageInput" name="base64Image" value="">
</form>

<form id="shotsList" class="row" action="$actionScript" method="post">

</form>
<div class="row">
	$postsList
</div>
$scriptCamera
CONTENT;

print_template('Editing room', true, $content);
?>
