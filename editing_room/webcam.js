/* ===================== SETUP ===================== */


/*
** setupWebcam:
**
** Use Webcam utility to set up the video and the canvas.
*/

function setupWebcam(self, stream)
{
    self.video = displayVideoElem(stream);
    self.video.addEventListener('loadedmetadata', () => {
        self.canvas.width = self.video.videoWidth;
        self.canvas.height = self.video.videoHeight;
    });
    self.video.addEventListener('play',function (stream){
        self._stream = stream;
        setupCanvas(self);
    });

		self.filter = null;
}

/*
** displayVideoElem:
**
** Store the this.video element.
** Create a video element an display the webcam stream on it.
*/

function displayVideoElem(stream)
{
    let video = document.createElement("video");
    video.srcObject = stream;
    video.onloadedmetadata = function (e){
        video.play();
    };
    return video;
}

/*
** setupCanvas:
**
** Play loop to draw the video stream on a canvas.
*/

function setupCanvas(self)
{
    var ctx = self.canvas.getContext("2d");
    var canvasImg = self._stream.target;
    (function loop(){
        if (!self._stream.paused && !self._stream.ended) {
            ctx.drawImage(canvasImg, 0, 0);
            setTimeout(loop, 1000/30);
        }
    })();
}

function stopCam(self)
{
    if (!self.haveCam)
        return ;
    self._stream.paused = true;
}

function startCam(self)
{
    if (self.haveCam == false)
        return ;
    delete self._stream.paused;
    self.canvas.width = self.video.videoWidth;
    self.canvas.height = self.video.videoHeight;
    setupCanvas(self);
}

/* ===================== INTERACTION ===================== */

/*
** Webcam:
**
** Represent the webcam stream, with his
** canvas representation.
*/
function Webcam()
{
    var self = this;
    self.video = null;
    self.canvas = document.querySelector("#canvas");
    self.haveCam = false;

	/*
    ** Prepare drag & drop event handler.
	*/
		let preventDefault = (evt) => {evt.preventDefault()};
    self.canvas.addEventListener("dragover", preventDefault);
    self.canvas.addEventListener("drop", function(dropEvent){
        dropEvent.preventDefault();
        if (dropEvent.dataTransfer.files.length)
            loadFile(self, dropEvent.dataTransfer);
    });

	/*
    ** Require the webcam acceptation to launch the stream.
    ** Display the webcam on a canvas of the webpage.
	*/
    let params = {
        audio:false,
        video : { width:720, height: 540 }
    };
	if (navigator.mediaDevices) {
		let webcamPromise = navigator.mediaDevices.getUserMedia(params);
		webcamPromise.then(function(stream, error){
			self.haveCam = true;
			setupWebcam(self, stream);
		});
	}

    /*
	** Setup webcam methods
    ** - takePics : Create a base64 image.
	*/
    self.takePics = function(){
        takePics(self);
    };
}

/*
** isBlankCanvas:
**
** Verify if the given canvas is blank.
** Create a new empty canvas and compare it with the given canvas.
*/

function isBlankCanvas(canvas)
{
    var blankCanvas = document.createElement("canvas");
    blankCanvas.width = canvas.width;
    blankCanvas.height = canvas.height;
    return (canvas.toDataURL() == blankCanvas.toDataURL());
}

function takePics(self)
{
    if (isBlankCanvas(self.canvas))
        return ;
	let imageShoot = document.getElementById('imageShoot');
	let img = self.canvas.toDataURL();
    let base64Image = img.replace("data:image/png;base64,", "");

	/*
	** Check picture size if PHP POST can receive the picture
	** and if is not too heavy for the database
	*/
	console.log(base64Image.length);
	if (base64Image.length >= 8388608 || base64Image.length >= 1309796) {
		alert("Image too heavy!")
		return ;
	}
	let filterInput = document.getElementById('filterInput');
	let imageInput = document.getElementById('imageInput');

	filterInput.value = self.canvas.dataset.filterName;
	imageInput.value = base64Image;
	console.log(imageShoot);
	imageShoot.submit();
}

function loadFile(self, fileItem)
{
	/*
	** Check if it is an image
	*/
	var fileToCheck = fileItem.files[0];
	var blob = fileToCheck;
	var fileReader = new FileReader();
	fileReader.onloadend = function(e) {
	  var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
	  var header = "";
	  for(var i = 0; i < arr.length; i++) {
	     header += arr[i].toString(16);
	  }

		/*
		** Check the file signature against known types
		*/
		var type = "";
		switch (header) {
	    case "89504e47":
	        type = "image/png";
	        break;
	    /* GIF is optionnal (don't work)
			case "47494638":
	        type = "image/gif";
	        break;
			*/
	    case "ffd8ffe0":
	    case "ffd8ffe1":
	    case "ffd8ffe2":
	    case "ffd8ffe3":
	    case "ffd8ffe8":
	        type = "image/jpeg";
	        break;
	    default:
	        type = "unknown";
	        break;
		}

		if (fileToCheck.type != type) {
			alert("The image need to be a PNG or JPEG/JPG");
			return ;
		}

	};

	/*
	** Add image to canvas
	*/
    if (self.haveCam && !self._stream.paused)
        self._stream.paused = true;
    let file = new FileReader();
    let image = new Image();
    let ctx = self.canvas.getContext("2d");
    file.onload = function (event){
        image.setAttribute("src", event.target.result);
    };
    image.onload = (event)=>{
        let image = event.target;
        self.canvas.width = image.width;
        self.canvas.height = image.height;
        ctx.drawImage(image, 0, 0);
    };
    let tranferedFile = fileItem.files[0];
    var data = file.readAsDataURL(tranferedFile);
}

export {Webcam};
