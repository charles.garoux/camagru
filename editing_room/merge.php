<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_gallery_posts.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/key_generator.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/image_position.php');

/*
** Recois une requet HTTP avec en POST :
**	base64Image (image de la camera en base64)
**	filterName	(nom du filtre)
**
** Cherche le filtre indique dans le fichier qui contient les filtres, si le filtre n'existe pas :
**	Afficher un avertissement
** Superposer le filtre choisi sur l'image envoye
**
** Enregistrement en BDD :
**	Nouveau post pour l'utilisateur connecte avec la nouvelle image
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if (Auth::is_authenticate() === false) {
	print_to_user('Warning', 'To make a montage you have to be authenticated');
	exit();
}

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode POST');
	exit();
}

$inputsList = [
	'base64Image' => ['value' => ($_POST['base64Image'] ?? NULL), 'type' => 'imageBase64'],
	'filterName' => ['value' => ($_POST['filterName'] ?? NULL), 'type' => 'str']
];

if (check_input($inputsList, $_POST) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$base64Image = $inputsList['base64Image']['value'];
$filterName =  $inputsList['filterName']['value'];
$filterPath = './filters/'. $filterName .'.png';
$tmpPath = 'tmp.link/';
$tmpFile = $tmpPath . key_generator() .'.png';

if (file_exists($filterPath) === false) {
	print_to_user('Warning', 'The filter "'. $filterName .'" does not exist');
	exit();
}

$imageDecoded = base64_decode($base64Image);
file_put_contents($tmpFile, $imageDecoded);

if (($resourceDest = imagecreatefrompng($tmpFile)) === false) {
	print_debug('ERREUR', __FILE__.':'.(__LINE__ - 1) .' imagecreatefrompng()');
	exit();
}
if (($resourceSrc = imagecreatefrompng($filterPath)) === false) {
	print_debug('ERREUR', __FILE__.':'.(__LINE__ - 1) .' imagecreatefrompng()');
	exit();
}

$position = get_center_position($tmpFile, $filterPath);
$filterSize = getimagesize($filterPath);

imagealphablending($resourceSrc, true);
imagecopy($resourceDest, $resourceSrc, $position[0], $position[1], 0, 0,  $filterSize[0], $filterSize[1]);

if ((imagepng($resourceDest, $tmpFile)) === false) {
	print_debug('ERREUR', '[FILESYSTEM]'.__FILE__.':'.(__LINE__ - 1) .' imagepng()');
	exit();
}

if (($newImage = file_get_contents($tmpFile)) === false) {
	print_debug('ERREUR', '[FILESYSTEM]'.__FILE__.':'.(__LINE__ - 1) .' file_get_contents()');
	exit();
}
$base64NewImage = base64_encode($newImage);

if (unlink($tmpFile) === false) {
	print_debug('ERREUR', '[FILESYSTEM]'.__FILE__.':'.(__LINE__ - 1) .' unlink()');
	exit();
}

if (($idPost = db_add_post($base64NewImage, $_SESSION['user']['id'])) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_post()');
	exit();
}

print_flash_page('Success', 'Your new post is created', null, 0);
?>
