import { Webcam } from "./webcam.js";

/*==================== INITIALISATION DE LA CAMERA ====================*/

var cam = new Webcam();

/*==================== CHANGEMENT DE FILTRE ====================*/

var changeFilter = function() {
	document.getElementById("shot").disabled = false;
	var filterName = this.getAttribute("data-filter-name");
	cam.canvas.setAttribute('data-filter-name', filterName);
};

/*==================== ACTIVATION DES BOUTONS ====================*/

var filters = document.getElementsByClassName("filter");
for (var i = 0; i < filters.length; i++) {
    filters[i].addEventListener('click', changeFilter);
}

document.querySelector("#shot").addEventListener("click", cam.takePics);
