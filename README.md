# Camagru

Premier **projet scolaire** de la branche Web de l'école [42 Lyon](https://www.42lyon.fr/).

## Objectif
L'on doit réaliser, en **PHP**, un petit site Instagram-like permettant à des utilisateurs de réaliser et partager des photo-montages.  

L'on doit ainsi implémenter, à mains nues (les frameworks sont interdits), les fonctionnalités de base rencontrées sur la majorité des sites possédant une base utilisateur.  

Les détails du projet sont décrits dans le [sujet](https://gitlab.com/charles.garoux/camagru/-/blob/master/docs/camagru.fr.pdf).

## Résultat

Le projet a été validé par 3 correcteurs durant la soutenance.

# Readme du rendu de projet

## Prerequis

1. Le fichier de debug (sont chemin etant specifier dans .env) doit avoir les droits d'ecriture pour tous le monde.

2. Creer sur le serveur de base de donnees la base de donnes "db_camagru" (ou un autre nom)

3. Creer le .env avec les donnees requises (voir section "Variable d'environnement de l'application").

4. Lancer les script de setup (/config/setup.php) par requete http avec les identifiant htaccess (voir section "htaccess").

## Variable d'environnement de l'application

Le fichier .env contient toutes les informations importantes pour le fonctionnement de l'application.

Differents fichiers d'exemples sont fournis pour aider a faire le .env :
- .env.exemple (contient un exemple fictif du fichier .env)
- .env.exemple.multi_case (contient les different cas de certaines variables)

Le fichier .env doit etre cree au moment de l'installation de l'application.

Voici les variables et ce a quoi elles correspondent :

```
APP_URL=adresse de l'application
APP_NAME=nom de l'application

SENDING_MAIL=mode de l'envoie
DEBUG=mode de l'envoie
CHECK_PASSWORD_STRENGTH=active/desactiver la verification de force du mot de passe

DB_CONNECTION=base de donnees
DB_HOST=adresse de la base de donnees
DB_PORT=port de la base de donnees
DB_DATABASE=nom de la base de donnees
DB_USERNAME=identifiant a la base de donnees
DB_PASSWORD=mot de passe de la base de donnees

HASH_ALGO=algorithme de hashage (compatible php)
ELEMENTS_LIMIT=nombre d'element par page de gallery

FLASH_WAIT=nombre de milisecondes d'attendre durant un "flash"

LOG_FILE_PATH=chemin absolue vers le fichier de journalisation
REQUEST_ID_LENGTH=la longueur de identifiant de requete (pour la journalisation)
```

## htaccess

Des fichiers htaccess bloque les fichiers et repertoire qui ne doivent pas etre accessible directement par un utilisateur.

Le fichier /config/setup.php est protege par des identifiants. Ces identifiants existent seulement pour la demo :
- Identifiant : admin
- Mot de passe : qwerty

Pour que les htaccess fonctionne il faut mettre "AllowOverride All" dans la configuration apache du serveur

## Documentations

Plusieurs fichiers de documentations sur le projet sont founie dans le repertoire docs :
- [Doc] API BDD : contient toutes les informations sur les fonctions de l'API cree pour le projet
- camagru.fr.pdf : est le sujet du projet

### Informations supplementaire pour la correction

En cas ou l'application n'accede pas a la camera cela peu venir de l'absence d'HTTPS. Pour corriger cela, sur Chrome il faut mettre en URL "[chrome://flags/#unsafely-treat-insecure-origin-as-secure](chrome://flags/#unsafely-treat-insecure-origin-as-secure)", y ajouter l'adresse de l'application puis relancer Chrome.
