<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');

/*
** Fomulaire de reinitialisation de mot de passe
**
** La verification de force du mot de passe peut etre active/desactive
** avec la variable d'environnement CHECK_PASSWORD_STRENGTH
*/

$actionScript = get_route_link('Password Reset Request');

$content = <<< CONTENT
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg my-5 bg-tan rounded">
            <div class="card-header text-center">
                Reset account password
            </div>
            <div class="card-body">
                <form action="$actionScript" method="post">
									<div class="form-group">
										<label for="login"><strong>Login :</strong></label>
										<input type="text" class="form-control" id="login" name="login" required>
									</div>
                    <button type="submit" id="submit" class="btn btn-primary">Reset</button>
                </form>
            </div>
        </div>
    </div>
</div>
CONTENT;

print_template('Reset account password', false, $content);

?>
