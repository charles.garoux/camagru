<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_reset.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_user_profile.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');

/*
** Recois une requet HTTP avec en POST :
**	key (clé du ticket de reinitialisation)
**	newPassword (le nouveau mot de passe de l'utilisateur)
**
** Lecture en BDD :
**	Utilisateur correspondant au "login"
** Mise a jour en BDD :
**	Mot de passe de l'utilisateur
** Suppression en BDD :
**	ticket de reinitialisation correspondant
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode POST');
	exit();
}

$inputsList = [
	'key' => ['value' => ($_POST['key'] ?? NULL), 'type' => 'str'],
	'newPassword' => ['value' => ($_POST['newPassword'] ?? NULL), 'type' => 'str']
];

if (check_input($inputsList, $_POST) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$key = $inputsList['key']['value'];
$newPassord = $inputsList['newPassword']['value'];

if (($resetTicket = db_find_resetTicket($key)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_resetTicket()');
	exit();
}
else if ($resetTicket == ReturnCase::Empty){
	print_to_user('Warning', 'The key '. $key .' does not exist');
	exit();
}

if (($user = db_find_user_by_idUser($resetTicket['idUser'])) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_user_by_idUser()');
	exit();
}

if (db_delete_resetTicket($resetTicket['id']) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_delete_resetTicket()');
	exit();
}

$passwordHashed = hash(env('HASH_ALGO'), $newPassord);

if (($res = db_update_user($user['id'], $user['login'], $passwordHashed, $user['email'], $user['receiveEmail'])) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_update_user()');
	exit();
}

print_flash_page('Success', 'Your new password is set', get_route_link('Sign In'));
?>
