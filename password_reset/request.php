<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_reset.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/key_generator.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/send_mail.php');

/*
** Recois une requet HTTP avec en POST :
**	login (identifiant de connection de l'utilisateur)
**
** Lecture en BDD :
**	Utilisateur correspondant au "login"
**
** Si l'utilisateur existe :
**	Creer un ticket de reinitialisation
**	Envoyer un mail contenant le lien pour reinitialiser
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode POST');
	exit();
}

$inputsList = [
	'login' => ['value' => ($_POST['login'] ?? NULL), 'type' => 'login']
];

if (check_input($inputsList, $_POST) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$login = $inputsList['login']['value'];

if (($user = db_find_user($login)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_user()');
	exit();
}
else if ($user === ReturnCase::Empty) {
	print_to_user('Warning', 'The user "'. $login .'" does not exist');
	exit();
}

$resetKey = key_generator();

if (db_add_resetTicket($user['id'], $resetKey) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_add_resetTicket()');
	exit();
}

$to = $user['email'];
$subject = 'Reset password at Camagru';
$message = 'Welcome, '. PHP_EOL ."Reset link : ". env('APP_URL') .'/password_reset/reset.php?key='. $resetKey;

if (send_mail($to, $subject, $message) === false){
	print_debug('ERREUR', '[MAIL]'.__FILE__.':'.(__LINE__ - 1));
	exit();
}

print_flash_page('Success', 'The request is send, check your email for password reset', get_route_link('Gallery'), 20000);
?>
