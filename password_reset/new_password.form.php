<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/env.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/includes/template.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/routes/routing.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');

/*
** Fomulaire de mise a jour de mot de passe
** Recois une requet HTTP avec en GET :
**	key (clé du ticket de reinitialisation)
**
** La verification de force du mot de passe peut etre active/desactive
** avec la variable d'environnement CHECK_PASSWORD_STRENGTH
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

$inputsList = [
	'key' => ['value' => ($_GET['key'] ?? NULL), 'type' => 'str']
];

if (check_input($inputsList, $_GET) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== CREATION DE LA PAGE =================== */

$key = $inputsList['key']['value'];

$actionScript = get_route_link('Password Reset Update');

if (env('CHECK_PASSWORD_STRENGTH') === 'TRUE') {
	$script = <<< SCRIPT

	let passwordForm = document.getElementById("password");
	let password_strength = document.getElementById("password_strength");
	document.getElementById("submit").disabled = false;

	passwordForm.oninput = CheckPasswordStrength;

	function CheckPasswordStrength(e) {
		var password = e.target.value;

		//if textBox is empty
		if(password.length == 0){
			password_strength.innerHTML = "";
			return;
		}

		//Regular Expressions
		var regex = new Array();
		regex.push("[A-Z]"); //For Uppercase Alphabet
		regex.push("[a-z]"); //For Lowercase Alphabet
		regex.push("[0-9]"); //For Numeric Digits
		regex.push("[$@$!%*#?&]"); //For Special Characters

		var score = 0;

		//Validation for each Regular Expression
		for (var i = 0; i < regex.length; i++) {
			if((new RegExp (regex[i])).test(password)){
				score++;
			}
		}

		//Validation for Length of Password
		if(score > 2 && password.length > 8){
			score++;
		}

		//Display of Status
		var color = "";
		var passwordStrengthText = "";
		var canSubmit = false;
		switch(score){
			case 0:
			canSubmit = false;
			break;
			case 1:
			passwordStrengthText = "Password is Weak.";
			color = "Red";
			canSubmit = false;
			break;
			case 2:
			passwordStrengthText = "Password is Good.";
			color = "darkorange";
			canSubmit = false;
			break;
			case 3:
			passwordStrengthText = "Password is Good.";
			color = "Green";
			canSubmit = true;
			break;
			case 4:
			passwordStrengthText = "Password is Strong.";
			color = "Green";
			canSubmit = true;
			break;
			case 5:
			passwordStrengthText = "Password is Very Strong.";
			color = "darkgreen";
			canSubmit = true;
			break;
		}
		document.getElementById("submit").disabled = !canSubmit;
		password_strength.innerHTML = passwordStrengthText;
		password_strength.style.color = color;
	}
SCRIPT;
}
else {
	$script = <<< SCRIPT
	let password_strength = document.getElementById("password_strength");
	password_strength.innerHTML = "";
SCRIPT;
}

$content = <<< CONTENT
<div class="row justify-content-center">
    <div class="col-xl-4 col-md-6 col-sm-12 col-12">
        <div class="card shadow-lg my-5 bg-tan rounded">
            <div class="card-header text-center">
                New password Datas
            </div>
            <div class="card-body">
                <form action="$actionScript" method="post">
									<input type="hidden" name="key" value="$key">
									<div class="form-group">
										<label for="password"><strong>Password :</strong></label> <span id="password_strength">Strength...</span>
										<input type="password" class="form-control" id="password" name="newPassword" required>
									</div>
                  <button type="submit" id="submit" class="btn btn-primary">Save new password</button>
                </form>
            </div>
        </div>
    </div>
</div>
CONTENT;

print_template('New password', false, $content, null, $script);

?>
