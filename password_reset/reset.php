<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/debug.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/Input.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/user_input_check.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_connection.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/db_api/db_users_reset.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/utils/message_to_user.php');

/*
** Recois une requet HTTP avec en GET :
**	key (clé du ticket de reinitialisation)
**
** Lecture en BDD :
**	Ticket de reinitialisation
**
** Si il y a un ticket valide, afficher le formulaire et y inclure la cle.
**
*/

/* ==================== VÉRIFICATION STANDARD ==================== */

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	print_debug_user_input(__FILE__, __LINE__, 'doit etre utilisé avec la méthode GET');
	exit();
}

$inputsList = [
	'key' => ['value' => ($_GET['key'] ?? NULL), 'type' => 'str']
];

if (check_input($inputsList, $_GET) === false){
	print_debug_user_input(__FILE__, __LINE__, 'check_input() === false');
	exit();
}

/* ================== TRAITEMENT DES DONNEES =================== */

$key = $inputsList['key']['value'];

if (($resetTicket = db_find_resetTicket($key)) === false) {
	print_debug_db(__FILE__, __LINE__, __FUNCTION__, 'db_find_resetTicket()');
	exit();
}
else if ($resetTicket == ReturnCase::Empty){
	print_to_user('Warning', 'The key '. $key .' does not exist');
	exit();
}

print_flash_page('Success', 'Valid key', get_route_link('Password Reset Update Form'). '?key='. $key);
?>
